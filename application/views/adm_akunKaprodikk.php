<!-- Sidebar -->
<div class="row">
		<div class="col-md-3">
			<nav class="sidebar">
				<div class="row nav">
					<div class="col-md-12">
						<p id="navhead"><b>Halo, <?php echo $this->session->userdata('nama'); ?>!</b></p>
					</div>
				</div>
				<a href="<?php echo base_url(); ?>Admin">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Dashboard</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/profil">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Profil Admin</p>
					</div>
				</div>
				</a>
				
				<div class="row nav1 active">
					<div class="col-md-12">
						<p id="nav">Akun Kaprodi / KK</p>
					</div>
				</div>
				
				<a href="<?php echo base_url(); ?>Admin/akunDosen">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Akun Dosen</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/jurusan">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Program Studi</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/mataKuliah">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Mata Kuliah</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/jadwalUjian">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Jadwal Ujian</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/soalUjian">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Soal Ujian</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/beritaAcara">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Berkas Berita Acara</p>
					</div>
				</div>
				</a>
			</nav>
		</div>

	<!-- Content -->	
	<div class="col-md-9 content">
		<br>
		<div class="row">
			<div class="col-md-9">
				<h4 id="title">Akun Kaprodi / KK</h4>
			</div>
			<div class="col-md-3">
				<a href="<?php echo base_url(); ?>Admin/tambahKaprodiKK"><button name="addKaprodiKK" class="btn btn-primary">Tambah Kaprodi/KK</button></a>
			</div>
			<!-- <div class="col-md-1"></div> -->
		</div>
		<br>
		<div class="row">
			<div class="col-md-11">
				<table class="table">
					<thead class="thead-light">
						<tr>
							<th>NIP</th>
							<th>Nama</th>
							<th>No. Telp</th>
							<th>Email</th>
							<th>Program Studi</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php  foreach($kaprodi as $kprd){ ?>
							<tr>
							<td><?php echo $kprd['NIP'] ?></td>
							<td><?php echo $kprd['nama'] ?></td>
							<td><?php echo $kprd['no_telp'] ?></td>
							<td><?php echo $kprd['email'] ?></td>
							<td><?php echo $kprd['nama_jur'] ?></td>
							<td><a href="<?php echo base_url(); ?>Admin/hapusKaprodiKK/<?php echo $kprd['id_kaprodikk'] ?>">Hapus</a></td>							
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
			<div class="col-md-1"></div>
		</div>
	</div>
</div>