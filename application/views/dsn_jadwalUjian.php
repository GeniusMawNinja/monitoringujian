<?php
	$id = "";
	foreach ($jadwal as $j){
		$id = $j['id_ujian'];
		$nama_mk = $j['nama_matkul'];
		$kode_mk = $j['kode_matkul'];
		$tgl = $j['tgl_ujian'];
		$periode = $j['jenis_ujian'] ." ". $j['semester'] ." ". $j['tahun_ajar'];
	}
?>
<!-- Sidebar -->
<div class="row">
		<div class="col-md-3">
			<nav class="sidebar">
				<div class="row nav">
					<div class="col-md-12">
						<p id="navhead"><b>Halo, <?php echo $this->session->userdata('nama'); ?>!</b></p>
					</div>
				</div>
				<a href="<?php echo base_url(); ?>Dosen">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Dashboard</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Dosen/profil">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Profil Dosen</p>
					</div>
				</div>
				</a>
				
				<div class="row nav1 active">
					<div class="col-md-12">
						<p id="nav">Jadwal Ujian</p>
					</div>
				</div>
				
				<a href="<?php echo base_url(); ?>Dosen/verifikasiSoal">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Soal Ujian</p>
					</div>
				</div>
				</a>
			</nav>
		</div>

	<!-- Content -->	
	<div class="col-md-9 content">
		<br>
		<div class="row">
			<div class="col-md-12">
				<h4 id="title">Jadwal Ujian</h4>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="panel">
				<div class="row">
					<div class="col-md-12">
						<p id="title"><b>Ujian yang akan datang : <?php if($id == "") { echo "Belum ada"; } ?></b></p>
					</div>
				</div>
				<?php if($id != "") { ?>
				<div class="row">
					<div class="col-md-12">
						<form method="POST" action="<?php echo base_url(); ?>Dosen/updateJadwal/<?php echo $id; ?>">
							<table cellpadding="8">
								<tr>
									<td width="175px"><label>Mata Kuliah </label></td>
									<td><label><?php echo $nama_mk; ?></label></td>
								</tr>
								<tr>
									<td><label>Kode Mata Kuliah </label></td>
									<td><label><?php echo $kode_mk; ?></label></td>
								</tr>
								<tr>
									<td><label>Tanggal Ujian </label></td>
									<td><label><?php echo $tgl; ?></label></td>
								</tr>
								<tr>
									<td><label>Periode Ujian </label></td>
									<td><label><?php echo $periode; ?></label></td>
								</tr>
								<tr>
									<td><label>Diujikan? </label></td>
									<td> <div class="btn-group btn-group-toggle" data-toggle="buttons">
									<label class="btn btn-secondary active">
										<input type="radio" name="ujian" id="ujian1" autocomplete="off" value="Ya" checked> Ya
									</label>
									<label class="btn btn-secondary">
										<input type="radio" name="ujian" id="ujian2" autocomplete="off" value="Tidak"> Tidak
									</label>
									</div></td>
								</tr>
								<tr>
									<td></td>
									<td><br><input type="submit" name="submit" id="submit" class="btn btn-success" value="Submit"></td>
								</tr>
							</table>
						</form> 
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>