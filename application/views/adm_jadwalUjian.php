<!-- Sidebar -->
<div class="row">
		<div class="col-md-3">
			<nav class="sidebar">
				<div class="row nav">
					<div class="col-md-12">
						<p id="navhead"><b>Halo, <?php echo $this->session->userdata('nama'); ?>!</b></p>
					</div>
				</div>
				<a href="<?php echo base_url(); ?>Admin">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Dashboard</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/profil">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Profil Admin</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/akunKaprodiKK">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Akun Kaprodi / KK</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/akunDosen">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Akun Dosen</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/jurusan">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Program Studi</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/mataKuliah">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Mata Kuliah</p>
					</div>
				</div>
				</a>
				
				<div class="row nav1 active">
					<div class="col-md-12">
						<p id="nav">Jadwal Ujian</p>
					</div>
				</div>
				
				<a href="<?php echo base_url(); ?>Admin/soalUjian">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Soal Ujian</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/beritaAcara">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Berkas Berita Acara</p>
					</div>
				</div>
				</a>
			</nav>
		</div>

	<!-- Content -->	
	<div class="col-md-9 content">
		<br>
			<div class="row">
				<div class="col-md-9">
					<h4 id="title">Jadwal Ujian</h4>
				</div>
				<div class="col-md-2">
					<a href="<?php echo base_url(); ?>Admin/tambahJadwal"><button name="addJadwal" class="btn btn-primary">Tambah Jadwal</button></a>
				</div>
				<div class="col-md-1"></div>
			</div>
			<br>
			<div class="row">
			<div class="col-md-11">
				<table class="table">
					<thead class="thead-light">
						<tr>
							<th>Kode Matkul</th>
							<th>Mata Kuliah</th>
							<th>Tgl. Ujian</th>
							<th>Periode</th>
							<th>Diujikan?</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
				<?php 	foreach($jadwal as $j) { ?>
						<tr>
							<td><?php echo $j['kode_matkul']; ?></td>
							<td><?php echo $j['nama_matkul']; ?></td>
							<td><?php echo $j['tgl_ujian']; ?></td>
							<td><?php echo $j['jenis_ujian'] ." ". $j['semester'] ." ". $j['tahun_ajar']; ?></td>
							<td><?php echo $j['ujian']; ?></td>
							<td><?php echo $j['status']; ?></td>
						</tr>
				<?php } ?>
					</tbody>
				</table>
			</div>
			<div class="col-md-1"></div>
		</div>
		<br><br><br>
	</div>
</div>