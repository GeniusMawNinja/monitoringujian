<?php
	foreach ($bav as $b){
		$id = $b['id'];
		$nama_mk = $b['matkul'];
		$kode_mk = $b['kode_mk'];
		$prodi = $b['prodi'];
		$tgl_bahas = $b['tgl_pembahasan'];
		$nama_kode = $b['matkul_kode'];
		$pilgan = $b['pilgan'];	
		$essay = $b['essay'];
		$lainnya = $b['lainnya'];
		$waktu = $b['waktu_ujian'];
		$peserta = $b['peserta_rapat'];
		$catatan = $b['catatan'];	
		$tgl = $b['tanggal'];
		$koor = $b['koordinator'];	
	}
?>
<!-- Sidebar -->
<div class="row">
		<div class="col-md-3">
			<nav class="sidebar">
				<div class="row nav">
					<div class="col-md-12">
						<p id="navhead"><b>Halo, <?php echo $this->session->userdata('nama'); ?>!</b></p>
					</div>
				</div>
				<a href="<?php echo base_url(); ?>Admin">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Dashboard</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/profil">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Profil Admin</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/akunKaprodiKK">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Akun Kaprodi / KK</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/akunDosen">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Akun Dosen</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/jurusan">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Program Studi</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/mataKuliah">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Mata Kuliah</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/jadwalUjian">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Jadwal Ujian</p>
					</div>
				</div>
				</a>
				
				<div class="row nav2 active">
					<div class="col-md-12">
						<p id="nav">Soal Ujian</p>
					</div>
				</div>
				
				<a href="<?php echo base_url(); ?>Admin/beritaAcara">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Berkas Berita Acara</p>
					</div>
				</div>
				</a>
			</nav>
		</div>

	<!-- Content -->	
	<div class="col-md-9 content">
		<br>
		<div class="row">
			<div class="col-md-12">
				<h4 id="title">Berita Acara</h4>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="panel">
				<div class="row">
					<div class="col-md-12">
						<p id="title"><b>Berita Acara Verifikasi Soal Ujian</b></p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<form method="POST" action="<?php echo base_url(); ?>Dosen/uploadBAV/<?php echo $id; ?>">
							<table cellpadding="8">
								<tr>
									<td width="300px"><label><b>Fakultas </b></label></td>
									<td><label><b>: Rekayasa Industri </b></label></td>
								</tr>
								<tr>
									<td><label>Saya sebagai Koordinator Mata Kuliah </label></td>
									<td><input type="text" class="form-control" name="matkul" value="<?php echo $nama_mk; ?>" required disabled></td>
								</tr>
								<tr>
									<td><label>Kode MK </label></td>
									<td><input type="text" class="form-control" name="kode_mk" value="<?php echo $kode_mk; ?>" required disabled></td>
								</tr>
								<tr>
									<td><label>Program Studi </label></td>
									<td><input type="text" class="form-control" name="prodi" value="<?php echo $prodi; ?>" required disabled></td>
								</tr>
								<tr>
									<td colspan="2"><label>Menyatakan bahwa telah dilakukan pembahasan antara Dosen Koordinator dan Dosen Mata Kuliah paralel : </label></td>
								</tr>
								<tr>
									<td><label>Tanggal Pembahasan </label></td>
									<td width="500px"><input type="date" class="form-control" name="tanggal" value="<?php echo $tgl_bahas; ?>" required disabled></td>
								</tr>
								<tr>
									<td><label>Mata Kuliah / Kode MK </label></td>
									<td><input type="text" class="form-control" name="nama/kode_mk" value="<?php echo $nama_mk ." / ". $kode_mk; ?>" required disabled></td>
								</tr>
								<tr>
									<td colspan="2"><label>Jumlah Soal : </label></td>
								</tr>
								<tr>
									<td><label>Pilihan Ganda </label></td>
									<td><input type="number" class="form-control" name="pilgan" value="<?php echo $pilgan; ?>" disabled></td>
								</tr>
								<tr>
									<td><label>Essay </label></td>
									<td><input type="number" class="form-control" name="essay" value="<?php echo $essay; ?>" disabled></td>
								</tr>
								<tr>
									<td><label>Lain-lain </label></td>
									<td><input type="number" class="form-control" name="lain" value="<?php echo $lainnya; ?>" disabled></td>
								</tr>
								<tr>
									<td colspan="2"></td>
								</tr>
								<tr>
									<td><label>Waktu Ujian (menit) </label></td>
									<td><input type="number" class="form-control" name="waktu" value="<?php echo $waktu; ?>" disabled></td>
								</tr>
								<tr>
									<td><label>Peserta Rapat Koordinasi </label></td>
									<td><textarea class="form-control" name="peserta" required disabled><?php echo $peserta; ?></textarea></td>
								</tr>
								<tr>
									<td colspan="2"><label>Soal ujian telah diverivikasi dan sesuai dengan tujuan kompetensi mata kuliah. Beban/alokasi waktu pengerjaan soal yang diberikan sesuai dengan tingkat kesulitan soal. </label></td>
								</tr>
								<tr>
									<td><label>Catatan </label></td>
									<td><textarea class="form-control" name="catatan" disabled><?php echo $catatan; ?></textarea></td>
								</tr>
								<tr>
									<td><label>Tanggal </label></td>
									<td><label>: <?php echo $tgl; ?></label></td>
								</tr>
								<tr>
									<td><label>Koordinator MK </label></td>
									<td><label>: <?php echo $koor; ?></label></td>
								</tr>
								<tr>
									<td></td>
									<td><br><a href="<?php echo base_url();?>Kaprodi/downloadBAV/<?php echo $id;?>"><button type="button" class="btn btn-success">Download BAV</button></a></td>
								</tr>
							</table>
						</form> 
						
					</div>
				</div>
			</div>
		</div>
		<br><br><br>
	</div>
</div>