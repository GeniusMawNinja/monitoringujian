<?php
	$id = "";
	if($done['status'] != "Selesai"){
		foreach ($ujian as $u){
			$id = $u['id_ujian'];
			$nama_mk = $u['nama_matkul'];
			$kode_mk = $u['kode_matkul'];
			$periode = $u['jenis_ujian'] ." Semester ". $u['semester'] ." Tahun Akademik ". $u['tahun_ajar'];
		}
	}

	foreach ($prodi as $p ){
		$id_jur = $p['id_jur'];
		$nama_jur = $p['nama_jur'];
	}
?>
<!-- Sidebar -->
<div class="row">
		<div class="col-md-3">
			<nav class="sidebar">
				<div class="row nav">
					<div class="col-md-12">
						<p id="navhead"><b>Halo, <?php echo $this->session->userdata('nama'); ?>!</b></p>
					</div>
				</div>
				<a href="<?php echo base_url(); ?>Dosen">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Dashboard</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Dosen/profil">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Profil Dosen</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Dosen/jadwalUjian">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Jadwal Ujian</p>
					</div>
				</div>
				</a>
				
				<div class="row nav2 active">
					<div class="col-md-12">
						<p id="nav">Soal Ujian</p>
					</div>
				</div>
				
			</nav>
		</div>

	<!-- Content -->	
	<div class="col-md-9 content">
		<br>
		<div class="row">
			<div class="col-md-12">
				<h4 id="title">Berita Acara</h4>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="panel">
				<?php if($id != "") { ?>
				<div class="row">
					<div class="col-md-12">
						<p id="title"><b>Berita Acara Penyerahan Soal <?php echo $periode; ?> </b></p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<form method="POST" action="<?php echo base_url(); ?>Dosen/uploadBAP/<?php echo $id; ?>">
							<table cellpadding="8">
								<tr>
									<td width="250px"><label><b>Fakultas </b></label></td>
									<td><label><b>: Rekayasa Industri </b></label></td>
								</tr>
								<tr>
									<td><label>Program Studi </label></td>
									<td><input type="text" class="form-control" name="prodi" value="<?php echo $nama_jur; ?>" required></td>
								</tr>
								<tr>
									<td><label>Kode MK </label></td>
									<td><input type="text" class="form-control" name="kode_mk" value="<?php echo $kode_mk; ?>" required></td>
								</tr>
								<tr>
									<td><label>Nama MK </label></td>
									<td><input type="text" class="form-control" name="matkul" value="<?php echo $nama_mk; ?>" required></td>
								</tr>
								<tr>
									<td><label>Koordinator MK </label></td>
									<td><input type="text" class="form-control" name="koordinator" value="<?php echo $this->session->userdata('nama'); ?>" required disabled></td>
								</tr>
								<tr>
									<td><label>Jenis Soal </label></td>
									<td>
										<div class="form-check form-check-inline"><input class="form-check-input" type="checkbox" name="jenis[]" value="Ya" id="jenis1"><label class="form-check-label" for="jenis1">Pilihan Ganda</label></div>
										<div class="form-check form-check-inline"><input class="form-check-input" type="checkbox" name="jenis[]" value="Ya" id="jenis2"><label class="form-check-label" for="jenis2">Isian</label></div>
									</td>
								</tr>
								<tr>
									<td><label>Tipe Soal </label></td>
									<td>
										<div class="form-check form-check-inline"><input class="form-check-input" type="checkbox" name="tipe[]" value="Ya" id="tipe1"><label class="form-check-label" for="tipe1">Tipe 1</label></div>
										<div class="form-check form-check-inline"><input class="form-check-input" type="checkbox" name="tipe[]" value="Ya" id="tipe2"><label class="form-check-label" for="tipe2">Tipe 2</label></div>
										<div class="form-check form-check-inline"><input class="form-check-input" type="checkbox" name="tipe[]" value="Ya" id="tipe3"><label class="form-check-label" for="tipe3">Susulan</label></div>
										<div class="form-check form-check-inline"><input class="form-check-input" type="checkbox" name="tipe[]" value="Ya" id="tipe4"><label class="form-check-label" for="tipe4">Remedial</label></div>
										<div class="form-check form-check-inline"><input class="form-check-input" type="checkbox" name="tipe[]" value="Ya" id="tipe5"><label class="form-check-label" for="tipe5">Lainnya</label></div>
									</td>
								</tr>
								<tr>
									<td><label>Tim Pembuat Soal </label></td>
									<td><textarea class="form-control" name="tim_pembuat" required></textarea></td>
								</tr>
								<tr>
									<td colspan="2"><label><b>SOAL YANG DISERAHKAN TELAH SESUAI DENGAN FORMAT DAN SIAP UNTUK DIGANDAKAN.</b></label></td>
								</tr>
								<tr>
									<td></td>
									<td><br><input type="submit" name="submit" id="submit" class="btn btn-success" value="Submit"></td>
								</tr>
							</table>
						</form> 
						
					</div>
				</div>
				<?php } else{ ?>
				<div class="row">
					<div class="col-md-12">
						<p id="title"><b>Proses monitoring ujian telah selesai, silahkan tunggu informasi selanjutnya.</b></p>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
		<br><br><br>
	</div>
</div>