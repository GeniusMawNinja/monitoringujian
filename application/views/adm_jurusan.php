<!-- Sidebar -->
<div class="row">
		<div class="col-md-3">
			<nav class="sidebar">
				<div class="row nav">
					<div class="col-md-12">
						<p id="navhead"><b>Halo, <?php echo $this->session->userdata('nama'); ?>!</b></p>
					</div>
				</div>
				<a href="<?php echo base_url(); ?>Admin">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Dashboard</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/profil">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Profil Admin</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/akunKaprodiKK">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Akun Kaprodi / KK</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/akunDosen">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Akun Dosen</p>
					</div>
				</div>
				</a>
				
				<div class="row nav1 active">
					<div class="col-md-12">
						<p id="nav">Program Studi</p>
					</div>
				</div>
				
				<a href="<?php echo base_url(); ?>Admin/mataKuliah">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Mata Kuliah</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/jadwalUjian">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Jadwal Ujian</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/soalUjian">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Soal Ujian</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/beritaAcara">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Berkas Berita Acara</p>
					</div>
				</div>
				</a>
			</nav>
		</div>

	<!-- Content -->	
	<div class="col-md-9 content">
		<br>
		<div class="row">
			<div class="col-md-9">
				<h4 id="title">List Program Studi</h4>
			</div>
			<div class="col-md-2">
				<a href="<?php echo base_url(); ?>Admin/tambahJurusan"><button name="addJurusan" class="btn btn-primary">Tambah Prodi</button></a>
			</div>
			<div class="col-md-1"></div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-11">
				<table class="table">
					<thead class="thead-light">
						<tr>
							<th>No</th>
							<th>Jurusan</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php $i = 1 ; foreach($jurusan as $jr) { ?>
						<tr>
							<td><?php echo $i ?></td>
							<td><?php echo $jr['nama_jur'] ?></td>
							<td><a href="<?php echo base_url(); ?>Admin/editJurusan/<?php echo $jr['id_jur'] ?>">Edit</a> / <a href="<?php echo base_url(); ?>Admin/hapusJurusan/<?php echo $jr['id_jur'] ?>">Hapus</a></td>
						</tr>	
						<?php $i++; }?>				
					</tbody>
				</table>
			</div>
			<div class="col-md-1"></div>
		</div>
		<br><br><br>
	</div>
</div>