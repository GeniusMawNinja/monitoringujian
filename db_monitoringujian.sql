-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 13, 2020 at 02:04 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_monitoringujian`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_admin`
--

CREATE TABLE `tb_admin` (
  `id` int(11) NOT NULL,
  `NIK` int(11) NOT NULL,
  `username` varchar(191) NOT NULL,
  `password` varchar(191) NOT NULL,
  `nama` varchar(191) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_admin`
--

INSERT INTO `tb_admin` (`id`, `NIK`, `username`, `password`, `nama`) VALUES
(1, 1234567890, 'admin', 'admin', 'Mr. Root');

-- --------------------------------------------------------

--
-- Table structure for table `tb_dosen`
--

CREATE TABLE `tb_dosen` (
  `id` int(11) NOT NULL,
  `NIP` int(20) NOT NULL,
  `nama_dsn` varchar(191) NOT NULL,
  `no_telp` varchar(15) NOT NULL,
  `email` varchar(191) NOT NULL,
  `username` varchar(191) NOT NULL,
  `password` varchar(191) NOT NULL,
  `id_matkul` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_dosen`
--

INSERT INTO `tb_dosen` (`id`, `NIP`, `nama_dsn`, `no_telp`, `email`, `username`, `password`, `id_matkul`) VALUES
(12, 1122334455, 'Pak Dosen', '081122334455', 'pakdosen@gmail.com', 'dosen', 'dosen', 1),
(14, 999948747, 'Bu Nia Ambarsari', '08123456789', 'bunia@gmail.com', 'bunia', 'bunia', 3);

-- --------------------------------------------------------

--
-- Table structure for table `tb_jurusan`
--

CREATE TABLE `tb_jurusan` (
  `id_jur` int(11) NOT NULL,
  `nama_jur` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_jurusan`
--

INSERT INTO `tb_jurusan` (`id_jur`, `nama_jur`) VALUES
(17, 'S1 Sistem Informasi'),
(20, 'S1 Teknik Industri'),
(21, 'S1 Teknik Logistik');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kaprodikk`
--

CREATE TABLE `tb_kaprodikk` (
  `id_kaprodikk` int(11) NOT NULL,
  `NIP` int(20) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `no_telp` varchar(15) NOT NULL,
  `email` varchar(100) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `id_jur` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_kaprodikk`
--

INSERT INTO `tb_kaprodikk` (`id_kaprodikk`, `NIP`, `nama`, `no_telp`, `email`, `username`, `password`, `id_jur`) VALUES
(1, 1223334444, 'Kaprodi SI', '081223334444', 'kaprodisi@gmail.com', 'kaprodisi', 'kaprodisi', 17),
(4, 99999888, 'Ajie Sukma W', '08123474621', 'ajie@gmail.com', 'ajie123', 'ajie123', 20);

-- --------------------------------------------------------

--
-- Table structure for table `tb_matkul`
--

CREATE TABLE `tb_matkul` (
  `id_matkul` int(11) NOT NULL,
  `kode_matkul` varchar(10) NOT NULL,
  `nama_matkul` varchar(100) NOT NULL,
  `id_jur` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_matkul`
--

INSERT INTO `tb_matkul` (`id_matkul`, `kode_matkul`, `nama_matkul`, `id_jur`) VALUES
(1, 'SI123ABC', 'Kalkulus 1b', 17),
(3, 'APSI1234', 'Analisis Perancangan Sistem Informasi', 17);

-- --------------------------------------------------------

--
-- Table structure for table `tb_penyerahansoal`
--

CREATE TABLE `tb_penyerahansoal` (
  `id` int(11) NOT NULL,
  `prodi` varchar(50) NOT NULL,
  `kode_mk` varchar(10) NOT NULL,
  `matkul` varchar(100) NOT NULL,
  `koordinator` varchar(100) NOT NULL,
  `pilgan` enum('Ya','Tidak') DEFAULT NULL,
  `isian` enum('Ya','Tidak') DEFAULT NULL,
  `tipe1` enum('Ya','Tidak') DEFAULT NULL,
  `tipe2` enum('Ya','Tidak') DEFAULT NULL,
  `susulan` enum('Ya','Tidak') DEFAULT NULL,
  `remedial` enum('Ya','Tidak') DEFAULT NULL,
  `lainnya` enum('Ya','Tidak') DEFAULT NULL,
  `pembuat_soal` text NOT NULL,
  `tanggal` date NOT NULL,
  `id_ujian` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_ujian`
--

CREATE TABLE `tb_ujian` (
  `id_ujian` int(11) NOT NULL,
  `id_matkul` int(11) NOT NULL,
  `tgl_ujian` date NOT NULL,
  `jenis_ujian` enum('UTS','UAS','Lainnya') NOT NULL,
  `semester` enum('Ganjil','Genap') NOT NULL,
  `tahun_ajar` varchar(15) NOT NULL,
  `ujian` enum('Ya','Tidak') DEFAULT NULL,
  `soal_docx` varchar(100) DEFAULT NULL,
  `soal_pdf` varchar(100) DEFAULT NULL,
  `status` enum('Disetujui','Ditolak','Menunggu','Diserahkan') NOT NULL,
  `komentar` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_verifikasisoal`
--

CREATE TABLE `tb_verifikasisoal` (
  `id` int(11) NOT NULL,
  `matkul` varchar(100) NOT NULL,
  `kode_mk` varchar(10) NOT NULL,
  `prodi` varchar(50) NOT NULL,
  `tgl_pembahasan` date NOT NULL,
  `matkul_kode` varchar(10) NOT NULL,
  `pilgan` int(5) DEFAULT NULL,
  `essay` int(5) DEFAULT NULL,
  `lainnya` int(5) DEFAULT NULL,
  `waktu_ujian` int(5) NOT NULL,
  `peserta_rapat` text NOT NULL,
  `catatan` text DEFAULT NULL,
  `koordinator` varchar(100) NOT NULL,
  `tanggal` date NOT NULL,
  `id_ujian` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_admin`
--
ALTER TABLE `tb_admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `NIK` (`NIK`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `tb_dosen`
--
ALTER TABLE `tb_dosen`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `NIP` (`NIP`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `fk_dosenmatkul` (`id_matkul`);

--
-- Indexes for table `tb_jurusan`
--
ALTER TABLE `tb_jurusan`
  ADD PRIMARY KEY (`id_jur`);

--
-- Indexes for table `tb_kaprodikk`
--
ALTER TABLE `tb_kaprodikk`
  ADD PRIMARY KEY (`id_kaprodikk`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `NIP` (`NIP`),
  ADD KEY `fk_kaprodikkjurusan` (`id_jur`);

--
-- Indexes for table `tb_matkul`
--
ALTER TABLE `tb_matkul`
  ADD PRIMARY KEY (`id_matkul`),
  ADD UNIQUE KEY `kode_matkul` (`kode_matkul`),
  ADD KEY `fk_jurmatkul` (`id_jur`);

--
-- Indexes for table `tb_penyerahansoal`
--
ALTER TABLE `tb_penyerahansoal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_penyerahanUjian` (`id_ujian`);

--
-- Indexes for table `tb_ujian`
--
ALTER TABLE `tb_ujian`
  ADD PRIMARY KEY (`id_ujian`),
  ADD KEY `fk_ujianmatkul` (`id_matkul`);

--
-- Indexes for table `tb_verifikasisoal`
--
ALTER TABLE `tb_verifikasisoal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_verifikasiUjian` (`id_ujian`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_admin`
--
ALTER TABLE `tb_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_dosen`
--
ALTER TABLE `tb_dosen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `tb_jurusan`
--
ALTER TABLE `tb_jurusan`
  MODIFY `id_jur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `tb_kaprodikk`
--
ALTER TABLE `tb_kaprodikk`
  MODIFY `id_kaprodikk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_matkul`
--
ALTER TABLE `tb_matkul`
  MODIFY `id_matkul` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_penyerahansoal`
--
ALTER TABLE `tb_penyerahansoal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_ujian`
--
ALTER TABLE `tb_ujian`
  MODIFY `id_ujian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tb_verifikasisoal`
--
ALTER TABLE `tb_verifikasisoal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_dosen`
--
ALTER TABLE `tb_dosen`
  ADD CONSTRAINT `fk_dosenmatkul` FOREIGN KEY (`id_matkul`) REFERENCES `tb_matkul` (`id_matkul`) ON UPDATE CASCADE;

--
-- Constraints for table `tb_kaprodikk`
--
ALTER TABLE `tb_kaprodikk`
  ADD CONSTRAINT `fk_kaprodikkjurusan` FOREIGN KEY (`id_jur`) REFERENCES `tb_jurusan` (`id_jur`) ON UPDATE CASCADE;

--
-- Constraints for table `tb_matkul`
--
ALTER TABLE `tb_matkul`
  ADD CONSTRAINT `fk_jurmatkul` FOREIGN KEY (`id_jur`) REFERENCES `tb_jurusan` (`id_jur`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_penyerahansoal`
--
ALTER TABLE `tb_penyerahansoal`
  ADD CONSTRAINT `fk_penyerahanUjian` FOREIGN KEY (`id_ujian`) REFERENCES `tb_ujian` (`id_ujian`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_ujian`
--
ALTER TABLE `tb_ujian`
  ADD CONSTRAINT `fk_ujianmatkul` FOREIGN KEY (`id_matkul`) REFERENCES `tb_matkul` (`id_matkul`) ON UPDATE CASCADE;

--
-- Constraints for table `tb_verifikasisoal`
--
ALTER TABLE `tb_verifikasisoal`
  ADD CONSTRAINT `fk_verifikasiUjian` FOREIGN KEY (`id_ujian`) REFERENCES `tb_ujian` (`id_ujian`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
