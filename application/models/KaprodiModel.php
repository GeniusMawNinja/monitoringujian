<?php
class KaprodiModel extends CI_Model
{


    public function getKaprodi()
    {
        $query= $this->db->get_where('tb_dosen',array('id'=>1));
        return $query;
    }

    public function get_all($table)
    {
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function get_where($table,$where)
    {
        $this->db->where($where);
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function get_where_numRows($table,$where)
    {
		$this->db->where($where);
        $query = $this->db->get($table);
        return $query->num_rows();
    }

    public function create($table,$data)
	{
		$query = $this->db->insert($table, $data);
		return $query;
	}
    
    function update($table,$data,$where)
    {
        $this->db->where($where);
        $query = $this->db->update($table, $data);
        return $query;
    }
    
    public function updateDosen($data){             
        $array = array(
            'nama_dsn'=>$data['nama_dsn'],
            'NIP'=>$data['NIP'],
            'no_telp'=>$data['no_telp'],
            'email'=>$data['email'],
            'id_matkul'=>$data['id_matkul'],
            'username'=>$data['username'],
            'password'=>$data['password']);         
            $this->db->set($array);
            $this->db->where('id',$data['id']);
            $this->db->update('tb_dosen');          
    }

    public function getProfil($where)
	{
		$query = $this->db->select('tb_jurusan.nama_jur , tb_kaprodikk.id_kaprodikk, tb_kaprodikk.nama, tb_kaprodikk.NIP, tb_kaprodikk.no_telp, tb_kaprodikk.email, tb_kaprodikk.username, tb_kaprodikk.password')
                 ->from('tb_kaprodikk')
                 ->join('tb_jurusan', 'tb_jurusan.id_jur = tb_kaprodikk.id_jur', 'inner')
                 ->where($where)
                 ->get();
        return $query->result_array();
	}

    public function getJadwal($where1,$where2)
    {
        $query = $this->db->select('tb_ujian.id_ujian, tb_matkul.kode_matkul, tb_matkul.nama_matkul, tb_ujian.tgl_ujian, tb_ujian.jenis_ujian, tb_ujian.semester, tb_ujian.tahun_ajar')
                 ->from('tb_ujian')
                 ->join('tb_matkul', 'tb_matkul.id_matkul = tb_ujian.id_matkul', 'inner')
                 ->where($where1)
                 ->where($where2)
                 ->get();
        return $query->result_array();
    }

    public function getSoal($where1,$where2)
    {
        $query = $this->db->select('tb_ujian.id_ujian, tb_jurusan.nama_jur, tb_matkul.kode_matkul, tb_matkul.nama_matkul, tb_ujian.tgl_ujian, tb_ujian.jenis_ujian, tb_ujian.semester, tb_ujian.tahun_ajar, tb_ujian.ujian, tb_ujian.status')
                 ->from('tb_ujian')
                 ->join('tb_matkul', 'tb_matkul.id_matkul = tb_ujian.id_matkul', 'inner')
                 ->join('tb_jurusan', 'tb_jurusan.id_jur = tb_matkul.id_jur', 'inner')
                 ->where($where1)
                 ->or_where($where2)
                 ->order_by('tb_ujian.id_ujian', 'DESC')
                 ->get();
        return $query->result_array();
    }

    public function getSoal2($where)
    {
        $query = $this->db->select('tb_ujian.id_ujian, tb_jurusan.nama_jur, tb_matkul.kode_matkul, tb_matkul.nama_matkul, tb_ujian.tgl_ujian, tb_ujian.jenis_ujian, tb_ujian.semester, tb_ujian.tahun_ajar, tb_ujian.ujian, tb_ujian.soal_docx, tb_ujian.soal_pdf, tb_ujian.status')
                 ->from('tb_ujian')
                 ->join('tb_matkul', 'tb_matkul.id_matkul = tb_ujian.id_matkul', 'inner')
                 ->join('tb_jurusan', 'tb_jurusan.id_jur = tb_matkul.id_jur', 'inner')
                 ->where($where)
                 ->get();
        return $query->result_array();
    }

    public function join_matkulJurusan($where)
    {
        $query = $this->db->select('tb_jurusan.id_jur, tb_jurusan.nama_jur')
                 ->from('tb_matkul')
                 ->join('tb_jurusan', 'tb_jurusan.id_jur = tb_matkul.id_jur', 'inner')
                 ->where($where)
                 ->get();
        return $query->result_array();
    }

}