<!-- Sidebar -->
<div class="row">
		<div class="col-md-3">
			<nav class="sidebar">
				<div class="row nav">
					<div class="col-md-12">
						<p id="navhead"><b>Halo, <?php echo $this->session->userdata('nama'); ?>!</b></p>
					</div>
				</div>
				
				<div class="row nav1 active">
					<div class="col-md-12">
						<p id="nav">Dashboard</p>
					</div>
				</div>
				
				<a href="<?php echo base_url(); ?>Dosen/profil">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Profil Dosen</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Dosen/jadwalUjian">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Jadwal Ujian</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Dosen/verifikasiSoal">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Soal Ujian</p>
					</div>
				</div>
				</a>
			</nav>
		</div>

	<!-- Content -->	
	<div class="col-md-9 content">
		<br>
		<div class="row">
			<div class="col-md-12">
				<h4 id="title"><b>Dashboard</b> Dosen</h4>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-12">
				<div class="box green">
					<br>
					<p id="box">Mata Kuliah</p>
					<br>
					<h2><b>SCM</b></h2>
				</div>
				<div class="box green">
					<br>
					<p id="box">Tanggal Ujian</p>
					<br>
					<h2><b>20/09/2020</b></h2>
				</div>
				<div class="box yellow">
					<br>
					<p id="box">Diujikan?</p>
					<br>
					<h2><b>Ya / Tidak</b></h2>
				</div>
			</div>
		</div>
		<br><br>
		<div class="row">
			<div class="col-md-12">
				<div class="box yellow">
					<br>
					<p id="box">Status Soal Ujian</p>
					<br>
					<h2><b>Menunggu</b></h2>
				</div>
				<div class="box red">
					<br>
					<p id="box">BA Verifikasi Soal</p>
					<br>
					<h2><b>Belum</b></h2>
				</div>
				<div class="box red">
					<br>
					<p id="box">BA Penyerahan Soal</p>
					<br>
					<h2><b>Belum</b></h2>
				</div>
			</div>
		</div>
	</div>
</div>