<!DOCTYPE html>
<html lang="en">
<head>
	<title>Monitoring Pelaksanaan Ujian - Login Page</title>
	<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" />
	<script src="<?php echo base_url(); ?>assets/jquery/jquery-3.4.1.slim.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
	<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/login.css" />
</head>
<body>
<div class="d-md-flex h-md-100 align-items-center">

	<!-- First Half -->

	<div class="col-md-4 p-0 h-md-100">
	    <div class="align-items-center bg-admin h-100 justify-content-center">
	    	<div class="row head bg-success">
	    		<h3>Monitoring Pelaksanaan Ujian</h3>
	    	</div>
	    	<div class="centered">
	        	<a href="<?php echo base_url(); ?>Login/admin"><button class="btn btn-dark awal"><h2>ADMIN</h2></button></a>
	        </div>
	    </div>
	</div>

	<!-- Second Half -->

	<div class="col-md-4 p-0 h-md-100">
	    <div class="align-items-center bg-kaprodi h-md-100 justify-content-center">
			<div class="row headc bg-success">
	    		<h3>Fakultas Rekayasa Industri</h3>
	    	</div>
	        <div class="centered">
	        	<a href="<?php echo base_url(); ?>Login/kaprodi"><button class="btn btn-dark awal"><h2>KAPRODI / KK</h2></button></a>
	        </div>
	        <div class="row footc bg-danger">
	        	<div class="col-md-12">
	        		<h3>T. Industri - S. Informasi - T. Logistik</h3>
	        	</div>
	        </div>
	    </div>
	</div>

	<div class="col-md-4 p-0 h-md-100">
	    <div class="align-items-center bg-dosen h-md-100 justify-content-center">
	        <div class="centered">
	        	<a href="<?php echo base_url(); ?>Login/dosen"><button class="btn btn-dark awal"><h2>DOSEN</h2></button></a>
	        </div>
	        <div class="row foot bg-danger">
	        	<div class="col-md-12">
	        		<h3>Powered By <b>Telkom University</b></h3>
	        	</div>
	        </div>
	    </div>
	</div>
    
</div>
</body>
</html>