<?php
	$ta = date("Y");
	$ta1 = date("Y")-1;
	$ta2 = date("Y")+1;
?>
<!-- Sidebar -->
<div class="row">
		<div class="col-md-3">
			<nav class="sidebar">
				<div class="row nav">
					<div class="col-md-12">
						<p id="navhead"><b>Halo, <?php echo $this->session->userdata('nama'); ?>!</b></p>
					</div>
				</div>
				<a href="<?php echo base_url(); ?>Admin">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Dashboard</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/profil">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Profil Admin</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/akunKaprodiKK">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Akun Kaprodi / KK</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/akunDosen">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Akun Dosen</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/jurusan">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Program Studi</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/mataKuliah">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Mata Kuliah</p>
					</div>
				</div>
				</a>
				
				<div class="row nav1 active">
					<div class="col-md-12">
						<p id="nav">Jadwal Ujian</p>
					</div>
				</div>
				
				<a href="<?php echo base_url(); ?>Admin/soalUjian">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Soal Ujian</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/beritaAcara">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Berkas Berita Acara</p>
					</div>
				</div>
				</a>
			</nav>
		</div>

	<!-- Content -->	
	<div class="col-md-9 content">
		<br>
		<div class="row">
			<div class="col-md-12">
				<h4 id="title">Tambah Jadwal Ujian</h4>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="panel">
				<div class="row">
					<div class="col-md-12">
						<p id="title"><b>Form Tambah Jadwal Ujian Baru</b></p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<form method="POST" action="<?php echo base_url(); ?>Admin/createJadwal">
							<table cellpadding="8">
								<tr>
									<td><label>Mata Kuliah </label></td>
									<td> <select class="form-control" name="matkul" required>
										<option value="" selected disabled>----- Pilih Mata Kuliah -----</option>
										<?php foreach($matkul as $mk){ ?>
										<option value="<?php echo $mk['id_matkul'] ?>"><?php echo $mk['nama_matkul'] ?></option>					
										<?php } ?>
									</select></td>
								</tr>
								<tr>
									<td><label>Tanggal Ujian </label></td>
									<td width="450px"> <input type="date" class="form-control" name="tanggal" required></td>
								</tr>
								<tr>
									<td><label>Jenis Ujian </label></td>
									<td> <div class="btn-group btn-group-toggle" data-toggle="buttons">
									<label class="btn btn-secondary active">
										<input type="radio" name="jenis" id="jenis1" autocomplete="off" value="UTS" checked> UTS
									</label>
									<label class="btn btn-secondary">
										<input type="radio" name="jenis" id="jenis2" autocomplete="off" value="UAS"> UAS
									</label>
									</div></td>
								</tr>
								<tr>
									<td><label>Semester </label></td>
									<td> <div class="btn-group btn-group-toggle" data-toggle="buttons">
									<label class="btn btn-secondary active">
										<input type="radio" name="semester" id="semester1" autocomplete="off" value="Ganjil" checked> Ganjil
									</label>
									<label class="btn btn-secondary">
										<input type="radio" name="semester" id="semester2" autocomplete="off" value="Genap"> Genap
									</label>
									</div></td>
								</tr>
								<tr>
									<td><label>Tahun Ajar </label></td>
									<td> <div class="btn-group btn-group-toggle" data-toggle="buttons">
									<label class="btn btn-secondary active">
										<input type="radio" name="tahun" id="tahun1" autocomplete="off" value="<?php echo $ta1 ."/". $ta; ?>" checked> <?php echo $ta1 ."/". $ta; ?>
									</label>
									<label class="btn btn-secondary">
										<input type="radio" name="tahun" id="tahun2" autocomplete="off" value="<?php echo $ta ."/". $ta2; ?>"> <?php echo $ta ."/". $ta2; ?>
									</label>
									</div></td>
								</tr>
								<tr>
									<td></td>
									<td><br><input type="submit" name="submit" id="submit" class="btn btn-success" value="Submit"> <input type="reset" name="cancel" id="reset" class="btn btn-secondary" value="Cancel"></td>
								</tr>
							</table>
						</form> 
					</div>
				</div>
			</div>
		</div>
	</div>
</div>