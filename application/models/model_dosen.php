<?php
class model_dosen extends CI_Model
{

	public function get_all($table)
    {
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function get_where($table,$where)
    {
		$this->db->where($where);
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function get_andWhere($table,$where1,$where2)
    {
        $this->db->where($where1);
        $this->db->where($where2);
        $query = $this->db->get($table);
        return $query->result_array();
    }

	public function get_where_numRows($table,$where)
    {
		$this->db->where($where);
        $query = $this->db->get($table);
        return $query->num_rows();
    }

    public function get_orWhere_numRows($table,$where1,$where2)
    {
		$this->db->where($where1);
		$this->db->or_where($where2);
        $query = $this->db->get($table);
        return $query->num_rows();
    }

    public function get_andWhere_numRows($table,$where1,$where2)
    {
        $this->db->where($where1);
        $this->db->where($where2);
        $query = $this->db->get($table);
        return $query->num_rows();
    }

	public function create($table,$data) {
		$query = $this->db->insert($table, $data);
		return $query;
	}

    function update($table,$data,$where)
    {
        $this->db->where($where);
        $query = $this->db->update($table, $data);
        return $query;
    }

    function update2($table,$data,$where1,$where2)
    {
        $this->db->where($where1);
        $this->db->where($where2);
        $query = $this->db->update($table, $data);
        return $query;
    }

    function delete($table,$where)
    {
        $query = $this->db->delete($table, $where);
        return $query;
    }

    public function join_where_profil($where) {
        $query = $this->db->select('tb_fakultas.id_fks, tb_jurusan.id_jur')
                 ->from('tb_dosen')
                 ->join('tb_peminatan', 'tb_peminatan.id_pmntn = tb_dosen.id_pmntn', 'inner')
                 ->join('tb_jurusan', 'tb_jurusan.id_jur = tb_peminatan.id_jur', 'inner')
                 ->join('tb_fakultas', 'tb_fakultas.id_fks = tb_jurusan.id_fks', 'inner')
                 ->where($where)
                 ->get();
        return $query->result_array();
    }

    public function join_where_mhsBimbingan($where) {
        $query = $this->db->select('tb_bimbingan.NIM, tb_mahasiswa.nama_mhs, tb_kelas.nama_kls, tb_bimbingan.tema, tb_bimbingan.status_bim')
                 ->from('tb_bimbingan')
                 ->join('tb_mahasiswa', 'tb_mahasiswa.NIM = tb_bimbingan.NIM', 'inner')
                 ->join('tb_kelas', 'tb_kelas.id_kelas = tb_mahasiswa.id_kel', 'inner')
                 ->where($where)
                 ->get();
        return $query->result_array();
    }

    public function join_where_detailMhs($where) {
        $query = $this->db->select('tb_mahasiswa.NIM, tb_mahasiswa.nama_mhs, tb_fakultas.nama_fks, tb_jurusan.nama_jur, tb_kelas.nama_kls, tb_peminatan.nama_pmntan, tb_bimbingan.tema, tb_bimbingan.file, tb_bimbingan.status_bim')
                 ->from('tb_mahasiswa')
                 ->join('tb_bimbingan', 'tb_bimbingan.NIM = tb_mahasiswa.NIM', 'inner')
                 ->join('tb_peminatan', 'tb_peminatan.id_pmntn = tb_mahasiswa.id_pmntn', 'inner')
                 ->join('tb_kelas', 'tb_kelas.id_kelas = tb_mahasiswa.id_kel', 'inner')
                 ->join('tb_jurusan', 'tb_jurusan.id_jur = tb_kelas.id_jur', 'inner')
                 ->join('tb_fakultas', 'tb_fakultas.id_fks = tb_jurusan.id_fks', 'inner')
                 ->where($where)
                 ->get();
        return $query->result_array();
    }

    public function join_where_detailTema($where) {
        $query = $this->db->select('tb_mahasiswa.NIM, tb_mahasiswa.nama_mhs, tb_kelas.nama_kls, tb_peminatan.nama_pmntan, tb_bimbingan.tema, tb_bimbingan.file, tb_bimbingan.status_tema, tb_bimbingan.catatan')
                 ->from('tb_bimbingan')
                 ->join('tb_mahasiswa', 'tb_mahasiswa.NIM = tb_bimbingan.NIM', 'inner')
                 ->join('tb_peminatan', 'tb_peminatan.id_pmntn = tb_mahasiswa.id_pmntn', 'inner')
                 ->join('tb_kelas', 'tb_kelas.id_kelas = tb_mahasiswa.id_kel', 'inner')
                 ->where($where)
                 ->get();
        return $query->result_array();
    }

    public function join_where_request2($where) {
        $query = $this->db->select('tb_dosen.NIP, tb_dosen.nama_dsn, tb_dosen.kuota')
                 ->from('tb_mahasiswa')
                 ->join('tb_peminatan', 'tb_peminatan.id_pmntn = tb_mahasiswa.id_pmntn', 'inner')
                 ->join('tb_dosen', 'tb_dosen.id_pmntn = tb_peminatan.id_pmntn', 'inner')
                 ->where($where)
                 ->get();
        return $query->result_array();
    }

    public function join_where_request3($where) {
        $query = $this->db->select('tb_dosen.nama_dsn, tb_bimbingan.catatan, tb_bimbingan.status_bim, tb_bimbingan.status_tema')
                 ->from('tb_bimbingan')
                 ->join('tb_dosen', 'tb_dosen.NIP = tb_dosen.NIP', 'inner')
                 ->where($where)
                 ->get();
        return $query->result_array();
    }

    public function join_where_tema($where) {
        $query = $this->db->select('tb_dosen.nama_dsn, tb_bimbingan.catatan, tb_bimbingan.status_bim, tb_bimbingan.status_tema')
                 ->from('tb_bimbingan')
                 ->join('tb_dosen', 'tb_dosen.NIP = tb_dosen.NIP', 'inner')
                 ->where($where)
                 ->get();
        return $query->result_array();
    }

    public function join_where_progressMhs($where) {
        $query = $this->db->select('tb_mahasiswa.NIM, tb_mahasiswa.nama_mhs, tb_kelas.nama_kls, tb_progress.status, tb_progress.id_prog')
                 ->from('tb_progress')
                 ->join('tb_timeline', 'tb_timeline.id_tmln = tb_progress.id_tmln', 'inner')
                 ->join('tb_mahasiswa', 'tb_mahasiswa.NIM = tb_progress.NIM', 'inner')
                 ->join('tb_kelas', 'tb_kelas.id_kelas = tb_mahasiswa.id_kel', 'inner')
                 ->where($where)
                 ->get();
        return $query->result_array();
    }

    public function join_where_detailProgressMhs($where) {
        $query = $this->db->select('tb_mahasiswa.NIM, tb_mahasiswa.nama_mhs, tb_kelas.nama_kls, tb_progress.status, tb_progress.id_prog, tb_progress.file, tb_bimbingan.tema')
                 ->from('tb_progress')
                 ->join('tb_mahasiswa', 'tb_mahasiswa.NIM = tb_progress.NIM', 'inner')
                 ->join('tb_kelas', 'tb_kelas.id_kelas = tb_mahasiswa.id_kel', 'inner')
                 ->join('tb_bimbingan', 'tb_bimbingan.NIM = tb_mahasiswa.NIM', 'inner')
                 ->where($where)
                 ->get();
        return $query->result_array();
    }

    public function join_where_timeline($where) {
        $query = $this->db->select('tb_timeline.id_tmln')
                 ->from('tb_progress')
                 ->join('tb_timeline', 'tb_timeline.id_tmln = tb_progress.id_tmln', 'inner')
                 ->where($where)
                 ->get();
        return $query->result_array();
    }

    public function join_where_skBimbingan($where1,$where2) {
        $query = $this->db->select()
                 ->from('tb_bimbingan')
                 ->join('tb_mahasiswa', 'tb_mahasiswa.NIM = tb_bimbingan.NIM', 'inner')
                 ->join('tb_kelas', 'tb_kelas.id_kelas = tb_mahasiswa.id_kel', 'inner')
                 ->where($where1)
                 ->where($where2)
                 ->get();
        return $query->result_array();
    }

}