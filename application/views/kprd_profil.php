<?php
foreach ($profil as $row) {
	$nip = $row['NIP'];
	$nama = $row['nama'];
	$telp = $row['no_telp'];
	$email = $row['email'];
	$username = $row['username'];
	$password = $row['password'];
	$jurusan = $row['nama_jur'];
}
?>
<!-- Sidebar -->
<div class="row">
		<div class="col-md-3">
			<nav class="sidebar">
				<div class="row nav">
					<div class="col-md-12">
						<p id="navhead"><b>Halo, <?php echo $this->session->userdata('nama'); ?>!</b></p>
					</div>
				</div>
				<a href="<?php echo base_url(); ?>Kaprodi">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Dashboard</p>
					</div>
				</div>
				</a>
				
				<div class="row nav2 active">
					<div class="col-md-12">
						<p id="nav">Profil Kaprodi / KK</p>
					</div>
				</div>
				
				<a href="<?php echo base_url(); ?>Kaprodi/ujian">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Ujian</p>
					</div>
				</div>
				</a>
			</nav>
		</div>	

	<!-- Content -->
	<div class="col-md-9 content">
		<br>
		<div class="row">
			<div class="col-md-12">
				<h4 id="title">Profil Kaprodi / KK</h4>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="panel">
				<div class="row">
					<div class="col-md-4">
						<img class="profile" src="<?php echo base_url(); ?>assets/icon/profile.png">
					</div>
					<div class="col-md-8">
						<form method="POST" action="<?php echo base_url(); ?>Kaprodi/updateProfil">
							<table cellpadding="8">
							<tr>
									<td><label>NIP </label></td>
									<td width="450px"> <input type="number" class="form-control" name="nip" value="<?php echo $nip; ?>" required></td>
								</tr>
								<tr>
									<td><label>Nama Lengkap </label></td>
									<td> <input type="text" class="form-control" name="nama" value="<?php echo $nama; ?>" required></td>
								</tr>
								<tr>
									<td><label>Nomor Telepon </label></td>
									<td><input type="text" class="form-control" name="telp" value="<?php echo $telp; ?>" disabled required></td>
								</tr>
								<tr>
									<td><label>Email </label></td>
									<td> <input type="email" class="form-control" name="email" value="<?php echo $email; ?>" required></td>
								</tr>
								<tr>
									<td><label>Jurusan </label></td>
									<td><input type="text" class="form-control" name="jurusan" value="<?php echo $jurusan; ?>" disabled required></td>
								</tr>
								<tr>
									<td><br><label>Username </label></td>
									<td><br> <input type="text" class="form-control" name="username" value="<?php echo $username; ?>" required></td>
								</tr>
								<tr>
									<td><label>Password </label></td>
									<td> <input type="password" class="form-control" name="password" value="<?php echo $password; ?>"></td>
								</tr>
								<tr>
									<td></td>
									<td><input type="submit" name="submit" id="submit" class="btn btn-success" value="Save"></td>
								</tr>
							</table>
						</form> 
					</div>
				</div>
			</div>
		</div>
		<br><br><br>
	</div>
</div>