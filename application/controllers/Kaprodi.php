<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kaprodi extends CI_Controller {

	public function __construct(){
		parent:: __construct();
		$this->load->helper(array('form', 'url', 'download'));
		$this->load->model('KaprodiModel');
		if($this->session->userdata('level') == '' || $this->session->userdata('level') != 'kaprodi'){
			redirect("Login/");
		}
	}

	public function index()
	{
		$table = "tb_jurusan";
		$where3 = array (
			'id_jur' => $this->session->userdata('jurusan')
		);
		$data['jurusan'] = $this->KaprodiModel->get_where($table,$where3);

		$this->load->view('kprd_header');
		$this->load->view('kprd_dashboard',$data);
		$this->load->view('kprd_footer');
	}

	public function profil()
	{
		$table = "tb_kaprodikk";
		$where = array (
			'id_kaprodikk' => $this->session->userdata('id'));
		$data['query'] = $this->KaprodiModel->get_where($table,$where);

		$data['profil'] = $this->KaprodiModel->getProfil($where); 

		$this->load->view('kprd_header');
		$this->load->view('kprd_profil',$data);
		$this->load->view('kprd_footer');
	}

	public function ujian()
	{
		$where1 = array (
			'tb_jurusan.id_jur' => $this->session->userdata('jurusan'),
			'tb_ujian.ujian' => "Ya",
			'tb_ujian.status' => "Diserahkan"
		);
		$where2 = array (
			'tb_jurusan.id_jur' => $this->session->userdata('jurusan'),
			'tb_ujian.ujian' => "Tidak"
		);
		$data['soal'] = $this->KaprodiModel->getSoal($where1,$where2);

		$table = "tb_jurusan";
		$where3 = array (
			'id_jur' => $this->session->userdata('jurusan')
		);
		$data['jurusan'] = $this->KaprodiModel->get_where($table,$where3);

		$this->load->view('kprd_header');
		$this->load->view('kprd_ujian',$data);
		$this->load->view('kprd_footer');
	}

	public function detailUjian($id)
	{
		$where = array (
			'tb_ujian.id_ujian' => $id
		);
		$data['ujian'] = $this->KaprodiModel->getSoal2($where);

		$this->load->view('kprd_header');
		$this->load->view('kprd_soalUjian',$data);
		$this->load->view('kprd_footer');
	}

	public function downloadDOCX($id)
	{
		$table = "tb_ujian";
		$where = array(
			'id_ujian' => $id
		);
		$data['soal'] = $this->KaprodiModel->get_where($table,$where);

		$file = realpath("soal_ujian") ."\\". $data['soal'][0]['soal_docx'];
		force_download($file, NULL);
	}

	public function downloadPDF($id)
	{
		$table = "tb_ujian";
		$where = array(
			'id_ujian' => $id
		);
		$data['soal'] = $this->KaprodiModel->get_where($table,$where);

		$file = realpath("soal_ujian") ."\\". $data['soal'][0]['soal_pdf'];
		force_download($file, NULL);
	}

	public function viewBAV($id)
	{
		$table = "tb_verifikasiSoal";
		$where = array(
			'id_ujian' => $id
		);
		$data['bav'] = $this->KaprodiModel->get_where($table,$where);

		$this->load->view('kprd_header');
		$this->load->view('kprd_verifikasiSoal',$data);
		$this->load->view('kprd_footer');
	}

	public function viewBAP($id)
	{
		$table = "tb_penyerahanSoal";
		$where = array(
			'id_ujian' => $id
		);
		$data['bap'] = $this->KaprodiModel->get_where($table,$where);

		$this->load->view('kprd_header');
		$this->load->view('kprd_penyerahanSoal',$data);
		$this->load->view('kprd_footer');
	}

	public function setujuUjian($id)
	{
		$table = "tb_ujian";
		$where = array(
			'id_ujian' => $id
		);
		$data = array (
			'status' => "Disetujui"
		);
		$this->KaprodiModel->update($table,$data,$where);
		redirect('Kaprodi/ujian');
	}

	public function tolakUjian($id)
	{
		$table = "tb_ujian";
		$where = array(
			'id_ujian' => $id
		);
		$data = array (
			'status' => "Ditolak",
			'soal_docx' => null,
			'soal_pdf' => null
		);
		$this->KaprodiModel->update($table,$data,$where);
	}

}