<?php 
	foreach($jurusan as $j){
		$jur = $j['nama_jur'];
	}
?>
<!-- Sidebar -->
<div class="row">
		<div class="col-md-3">
			<nav class="sidebar">
				<div class="row nav">
					<div class="col-md-12">
						<p id="navhead"><b>Halo, <?php echo $this->session->userdata('nama'); ?>!</b></p>
					</div>
				</div>
				
				<div class="row nav1 active">
					<div class="col-md-12">
						<p id="nav">Dashboard</p>
					</div>
				</div>
				
				<a href="<?php echo base_url(); ?>Kaprodi/profil">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Profil Kaprodi / KK</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Kaprodi/ujian">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Ujian</p>
					</div>
				</div>
				</a>
			</nav>
		</div>	

	<!-- Content -->
	<div class="col-md-9 content">
		<br>
		<div class="row">
			<div class="col-md-12">
				<h4 id="title"><b>Dashboard</b> Kaprodi <?php echo $jur; ?></h4>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-12">
				<div class="box blue">
					<br>
					<p id="box">Jumlah Mata Kuliah</p>
					<br>
					<h3><b>16</b></h3>
				</div>

				<div class="box blue">
					<br>
					<p id="box">Mata Kuliah Diujikan</p>
					<br>
					<h3><b>10</b></h3>
				</div>
			</div>
		</div>
	</div>
</div>