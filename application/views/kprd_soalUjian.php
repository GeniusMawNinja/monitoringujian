<?php
	foreach ($ujian as $u){
		$id = $u['id_ujian'];
		$jurusan = $u['nama_jur'];
		$nama_mk = $u['nama_matkul'];
		$kode_mk = $u['kode_matkul'];
		$tgl = $u['tgl_ujian'];
		$uji = $u['ujian'];
		$docx = $u['soal_docx'];
		$pdf = $u['soal_pdf'];
		$status = $u['status'];
		$periode = $u['jenis_ujian'] ." ". $u['semester'] ." ". $u['tahun_ajar'];
	}
?>
<!-- Sidebar -->
<div class="row">
		<div class="col-md-3">
			<nav class="sidebar">
				<div class="row nav">
					<div class="col-md-12">
						<p id="navhead"><b>Halo, <?php echo $this->session->userdata('nama'); ?>!</b></p>
					</div>
				</div>
				<a href="<?php echo base_url(); ?>Kaprodi">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Dashboard</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Kaprodi/profil">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Profil Kaprodi / KK</p>
					</div>
				</div>
				</a>
				
				<div class="row nav1 active">
					<div class="col-md-12">
						<p id="nav">Ujian</p>
					</div>
				</div>
				
			</nav>
		</div>

	<!-- Content -->	
	<div class="col-md-9 content">
		<br>
		<div class="row">
			<div class="col-md-12">
				<h4 id="title">Soal Ujian</h4>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="panel">
				<div class="row">
					<div class="col-md-12">
						<p id="title"><b>Soal Ujian <?php echo $periode; ?></b></p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<!-- <form method="POST" action="<?php echo base_url();?>Kaprodi/approveUjian/<?php echo $id;?>"> -->
							<table cellpadding="8">
								<tr>
									<td width="175px"><label>Jurusan </label></td>
									<td><label><?php echo $jurusan; ?></label></td>
								</tr>
								<tr>
									<td><label>Mata Kuliah </label></td>
									<td><label><?php echo $nama_mk; ?></label></td>
								</tr>
								<tr>
									<td><label>Kode MK </label></td>
									<td><label><?php echo $kode_mk; ?></label></td>
								</tr>
								<tr>
									<td><label>Tanggal Ujian </label></td>
									<td><label><?php echo $tgl; ?></label></td>
								</tr>
								<tr>
									<td><label>Periode Ujian </label></td>
									<td><label><?php echo $periode; ?></label></td>
								</tr>
								<tr>
									<td><label>Soal DOCX </label></td>
									<td width="600px"><a href="<?php echo base_url();?>Kaprodi/downloadDOCX/<?php echo $id;?>"><button type="button" class="btn btn-secondary">Lihat Soal</button></a> <label><?php echo $docx; ?></label></td>
								</tr>
								<tr>
									<td><label>Soal PDF </label></td>
									<td><a href="<?php echo base_url();?>Kaprodi/downloadPDF/<?php echo $id;?>"><button type="button" class="btn btn-secondary">Lihat Soal</button></a> <label><?php echo $pdf; ?></label></td>
								</tr>
								<tr>
									<td><label>BA Verifikasi Soal </label></td>
									<td width="600px"><a href="<?php echo base_url();?>Kaprodi/viewBAV/<?php echo $id;?>"><button type="button" class="btn btn-secondary">Lihat Berita Acara</button></a></td>
								</tr>
								<tr>
									<td><label>BA Penyerahan Soal </label></td>
									<td><a href="<?php echo base_url();?>Kaprodi/viewBAP/<?php echo $id;?>"><button type="button" class="btn btn-secondary">Lihat Berita Acara</button></a></td>
								</tr>
								<tr>
									<td></td>
									<td><br><a href="<?php echo base_url();?>Kaprodi/setujuUjian/<?php echo $id;?>"><button type="button" class="btn btn-success" <?php if($status=="Disetujui"){echo "disabled";}?>>Setujui</button></a>
									<a href="<?php echo base_url();?>Kaprodi/tolakUjian/<?php echo $id;?>"><button type="button" class="btn btn-danger" <?php if($status=="Disetujui"){echo "disabled";}?>>Tolak</button></a></td>
								</tr>
							</table>
						<!-- </form>  -->
					</div>
				</div>
			</div>
		</div>
		<br><br><br>
	</div>
</div>