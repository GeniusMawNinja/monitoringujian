<?php
class model_kaprodi extends CI_Model
{

	public function get_all($table)
    {
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function get_where($table,$where)
    {
		$this->db->where($where);
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function get_andWhere($table,$where1,$where2)
    {
        $this->db->where($where1);
        $this->db->where($where2);
        $query = $this->db->get($table);
        return $query->result_array();
    }

	public function get_where_numRows($table,$where)
    {
		$this->db->where($where);
        $query = $this->db->get($table);
        return $query->num_rows();
    }

    public function get_orWhere_numRows($table,$where1,$where2)
    {
		$this->db->where($where1);
		$this->db->or_where($where2);
        $query = $this->db->get($table);
        return $query->num_rows();
    }

    public function get_andWhere_numRows($table,$where1,$where2)
    {
        $this->db->where($where1);
        $this->db->where($where2);
        $query = $this->db->get($table);
        return $query->num_rows();
    }

	public function create($table,$data) {
		$query = $this->db->insert($table, $data);
		return $query;
	}

    function update($table,$data,$where)
    {
        $this->db->where($where);
        $query = $this->db->update($table, $data);
        return $query;
    }

    public function join_where_profil($where) {
        $query = $this->db->select('tb_fakultas.id_fks, tb_jurusan.id_jur, tb_kelas.id_kelas')
                 ->from('tb_mahasiswa')
                 ->join('tb_kelas', 'tb_kelas.id_kelas = tb_mahasiswa.id_kel', 'inner')
                 ->join('tb_jurusan', 'tb_jurusan.id_jur = tb_kelas.id_jur', 'inner')
                 ->join('tb_fakultas', 'tb_fakultas.id_fks = tb_jurusan.id_fks', 'inner')
                 ->where($where)
                 ->get();
        return $query->result_array();
    }

    public function join_where_request($where) {
        $query = $this->db->select('tb_fakultas.nama_fks, tb_jurusan.nama_jur, tb_kelas.nama_kls, tb_peminatan.nama_pmntan')
                 ->from('tb_mahasiswa')
                 ->join('tb_peminatan', 'tb_peminatan.id_pmntn = tb_mahasiswa.id_pmntn', 'inner')
                 ->join('tb_kelas', 'tb_kelas.id_kelas = tb_mahasiswa.id_kel', 'inner')
                 ->join('tb_jurusan', 'tb_jurusan.id_jur = tb_kelas.id_jur', 'inner')
                 ->join('tb_fakultas', 'tb_fakultas.id_fks = tb_jurusan.id_fks', 'inner')
                 ->where($where)
                 ->get();
        return $query->result_array();
    }

    public function join_where_request2($where) {
        $query = $this->db->select('tb_dosen.NIP, tb_dosen.nama_dsn, tb_dosen.kuota')
                 ->from('tb_mahasiswa')
                 ->join('tb_peminatan', 'tb_peminatan.id_pmntn = tb_mahasiswa.id_pmntn', 'inner')
                 ->join('tb_dosen', 'tb_dosen.id_pmntn = tb_peminatan.id_pmntn', 'inner')
                 ->where($where)
                 ->get();
        return $query->result_array();
    }

    public function join_where_request3($where) {
        $query = $this->db->select('tb_dosen.nama_dsn, tb_bimbingan.catatan, tb_bimbingan.status_bim')
                 ->from('tb_bimbingan')
                 ->join('tb_dosen', 'tb_dosen.NIP = tb_bimbingan.NIP', 'inner')
                 ->where($where)
                 ->get();
        return $query->result_array();
    }

    public function join_where_tema($where) {
        $query = $this->db->select('tb_dosen.nama_dsn, tb_bimbingan.catatan, tb_bimbingan.status_bim')
                 ->from('tb_bimbingan')
                 ->join('tb_dosen', 'tb_dosen.NIP = tb_bimbingan.NIP', 'inner')
                 ->where($where)
                 ->get();
        return $query->result_array();
    }

    public function join_where_timeline_numRows($where) {
        $query = $this->db->select()
                 ->from('tb_timeline')
                 ->join('tb_dosen', 'tb_dosen.NIP = tb_timeline.NIP', 'inner')
                 ->join('tb_bimbingan', 'tb_bimbingan.NIP = tb_dosen.NIP', 'inner')
                 ->join('tb_mahasiswa', 'tb_mahasiswa.NIM = tb_bimbingan.NIM', 'inner')
                 ->where($where)
                 ->get();
        return $query->num_rows();
    }

    public function join_where_timeline($where) {
        $query = $this->db->select()
                 ->from('tb_timeline')
                 ->join('tb_dosen', 'tb_dosen.NIP = tb_timeline.NIP', 'inner')
                 ->join('tb_bimbingan', 'tb_bimbingan.NIP = tb_dosen.NIP', 'inner')
                 ->join('tb_mahasiswa', 'tb_mahasiswa.NIM = tb_bimbingan.NIM', 'inner')
                 ->where($where)
                 ->get();
        return $query->result_array();
    }

    public function join_where_progress($where1,$where2) {
        $query = $this->db->select()
                 ->from('tb_progress')
                 ->join('tb_timeline', 'tb_timeline.id_tmln = tb_progress.id_tmln', 'inner')
                 ->where($where1)
                 ->where($where2)
                 ->get();
        return $query->result_array();
    }

}