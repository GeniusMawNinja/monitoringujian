<!-- Sidebar -->
<div class="row">
		<div class="col-md-3">
			<nav class="sidebar">
				<div class="row nav">
					<div class="col-md-12">
						<p id="navhead"><b>Halo, <?php echo $this->session->userdata('nama'); ?>!</b></p>
					</div>
				</div>
				<a href="<?php echo base_url(); ?>Admin">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Dashboard</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/profil">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Profil Admin</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/akunKaprodiKK">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Akun Kaprodi / KK</p>
					</div>
				</div>
				</a>
				
				<div class="row nav2 active">
					<div class="col-md-12">
						<p id="nav">Akun Dosen</p>
					</div>
				</div>
				
				<a href="<?php echo base_url(); ?>Admin/jurusan">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Program Studi</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/mataKuliah">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Mata Kuliah</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/jadwalUjian">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Jadwal Ujian</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/soalUjian">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Soal Ujian</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/beritaAcara">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Berkas Berita Acara</p>
					</div>
				</div>
				</a>
			</nav>
		</div>

	<!-- Content -->	
	<div class="col-md-9 content">
		<br>
		<div class="row">
			<div class="col-md-12">
				<h4 id="title">Tambah Dosen</h4>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="panel">
				<div class="row">
					<div class="col-md-12">
						<p id="title"><b>Form Tambah Dosen Baru</b></p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<form method="POST" action="<?php echo base_url(); ?>Admin/createDosen">
							<table cellpadding="8">
							<tr>
									<td><label>NIP </label></td>
									<td width="450px"> <input type="number" class="form-control" name="nip" required></td>
								</tr>
								<tr>
									<td><label>Nama Lengkap </label></td>
									<td> <input type="text" class="form-control" name="nama" required></td>
								</tr>
								<tr>
									<td><label>No. Telepon </label></td>
									<td width="450px"> <input type="text" class="form-control" name="telp" required></td>
								</tr>
								<tr>
									<td><label>Email </label></td>
									<td> <input type="email" class="form-control" name="email" required></td>
								</tr>
								<tr>
									<td><label>Username </label></td>
									<td> <input type="text" class="form-control" name="username" required></td>
								</tr>
								<tr>
									<td><label>Password </label></td>
									<td> <input type="password" class="form-control" name="password" required></td>
								</tr>
								<tr>
									<td><label>Mata Kuliah </label></td>
									<td> <select class="form-control" name="matkul" id="matkul" required>
										<option value="" selected disabled>----- Pilih Mata Kuliah -----</option>
										<?php
										foreach ($matkul as $row){
										?>
			                            <option value="<?php echo $row['id_matkul']; ?>" > <?php echo $row['nama_matkul'] ?></option>
			                            <?php
			                            }
										?>
									</select></td>
								</tr>
								<tr>
									<td></td>
									<td><input type="submit" name="submit" id="submit" class="btn btn-success" value="Submit"> <input type="reset" name="cancel" id="reset" class="btn btn-secondary" value="Cancel"></td>
								</tr>
							</table>
						</form> 
					</div>
				</div>
			</div>
		</div>
		<br><br><br>
	</div>
</div>