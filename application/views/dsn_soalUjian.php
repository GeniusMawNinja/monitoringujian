<?php
	$id = "";
	foreach ($soal as $s){
		$id = $s['id_ujian'];
		$nama_mk = $s['nama_matkul'];
		$kode_mk = $s['kode_matkul'];
		$tgl = $s['tgl_ujian'];
		$periode = $s['jenis_ujian'] ." ". $s['semester'] ." ". $s['tahun_ajar'];
		$komentar = $s['komentar'];
	}
?>
<!-- Sidebar -->
<div class="row">
		<div class="col-md-3">
			<nav class="sidebar">
				<div class="row nav">
					<div class="col-md-12">
						<p id="navhead"><b>Halo, <?php echo $this->session->userdata('nama'); ?>!</b></p>
					</div>
				</div>
				<a href="<?php echo base_url(); ?>Dosen">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Dashboard</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Dosen/profil">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Profil Dosen</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Dosen/jadwalUjian">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Jadwal Ujian</p>
					</div>
				</div>
				</a>
				
				<div class="row nav2 active">
					<div class="col-md-12">
						<p id="nav">Soal Ujian</p>
					</div>
				</div>
				
			</nav>
		</div>

	<!-- Content -->	
	<div class="col-md-9 content">
		<br>
		<div class="row">
			<div class="col-md-12">
				<h4 id="title">Soal Ujian</h4>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="panel">
				<?php if($id != "") { ?>
				<div class="row">
					<div class="col-md-12">
						<p id="title"><b>Silahkan upload soal ujian yang akan datang.</b></p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<?php echo form_open_multipart('Dosen/uploadSoal/'.$id);?>
							<table cellpadding="8">
								<tr>
									<td width="175px"><label>Mata Kuliah </label></td>
									<td><label><?php echo $nama_mk; ?></label></td>
								</tr>
								<tr>
									<td><label>Kode Mata Kuliah </label></td>
									<td><label><?php echo $kode_mk; ?></label></td>
								</tr>
								<tr>
									<td><label>Tanggal Ujian </label></td>
									<td><label><?php echo $tgl; ?></label></td>
								</tr>
								<tr>
									<td><label>Periode Ujian </label></td>
									<td><label><?php echo $periode; ?></label></td>
								</tr>
								<tr>
									<td><label>Soal DOCX </label></td>
									<td width="600px"><div class="custom-file">
										<input type="file" class="custom-file-input" name="soalDOCX" id="soalDOCX" required>
										<label class="custom-file-label" for="soalDOCX">Choose file</label>
									</div></td>
								</tr>
								<tr>
									<td><label>Soal PDF </label></td>
									<td><div class="custom-file">
										<input type="file" class="custom-file-input" name="soalPDF" id="soalPDF" required>
										<label class="custom-file-label" for="soalPDF">Choose file</label>
									</div></td>
								</tr>
								<tr>
									<td></td>
									<td><br><input type="submit" name="submit" id="submit" class="btn btn-success" value="Submit"></td>
								</tr>
							</table>
						</form> 
					</div>
				</div>
				<?php } else{ ?>
				<div class="row">
					<div class="col-md-12">
						<p id="title"><b>Proses monitoring ujian telah selesai, silahkan tunggu informasi selanjutnya.</b></p>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>

<script>
// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});
</script>