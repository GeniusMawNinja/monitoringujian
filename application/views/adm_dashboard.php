<!-- Sidebar -->
<div class="row">
		<div class="col-md-3">
			<nav class="sidebar">
				<div class="row nav">
					<div class="col-md-12">
						<p id="navhead"><b>Halo, <?php echo $this->session->userdata('nama'); ?>!</b></p>
					</div>
				</div>
				
				<div class="row nav1 active">
					<div class="col-md-12">
						<p id="nav">Dashboard</p>
					</div>
				</div>
				
				<a href="<?php echo base_url(); ?>Admin/profil">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Profil Admin</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/akunKaprodiKK">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Akun Kaprodi/KK</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/akunDosen">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Akun Dosen</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/jurusan">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Program Studi</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/mataKuliah">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Mata Kuliah</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/jadwalUjian">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Jadwal Ujian</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/soalUjian">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Soal Ujian</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/beritaAcara">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Berkas Berita Acara</p>
					</div>
				</div>
				</a>
			</nav>
		</div>

	<!-- Content -->	
	<div class="col-md-9 content">
		<br>
		<div class="row">
			<div class="col-md-12">
				<h4 id="title"><b>Dashboard</b> Admin FRI</h4>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-12">
				<div class="box grey">
					<br>
					<p id="box">Jumlah Akun Kaprodi/KK</p>
					<br>
					<h1><b>1</b></h1>
				</div>
				<div class="box grey">
					<br>
					<p id="box">Jumlah Akun Dosen</p>
					<br>
					<h1><b>10</b></h1>
				</div>
				<div class="box grey">
					<br>
					<p id="box">Jumlah Program Studi</p>
					<br>
					<h1><b>3</b></h1>
				</div>
			</div>
		</div>
		<br><br>
		<div class="row">
			<div class="col-md-12">
				<div class="box grey">
					<br>
					<p id="box">Jumlah Mata Kuliah</p>
					<br>
					<h1><b>3</b></h1>
				</div>
				<div class="box grey">
					<br>
					<p id="box">Jumlah Jadwal Ujian</p>
					<br>
					<h1><b>3</b></h1>
				</div>
				<div class="box grey">
					<br>
					<p id="box">Jumlah Soal Ujian</p>
					<br>
					<h1><b>3</b></h1>
				</div>
			</div>
		</div>
		<br><br>
		<div class="row">
			<div class="col-md-12">
				
			</div>
		</div>
	</div>
</div>