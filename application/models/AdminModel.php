<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminModel extends CI_Model {

	public function getAdmin()
	{
		$query= $this->db->get_where('tb_admin',array('id'=>1));
		return $query;
	}

	public function get_all($table)
    {
        $query = $this->db->get($table);
        return $query->result_array();
	}

	public function get_where($table,$where)
    {
		$this->db->where($where);
        $query = $this->db->get($table);
        return $query->result_array();
	}
	
	function update($table,$data,$where)
    {
        $this->db->where($where);
        $query = $this->db->update($table, $data);
        return $query;
    }
	
	public function updateAdmin($data){				
		$array = array(
			'nama'=>$data['nama'],
			'NIK'=>$data['NIK'],
			'username'=>$data['username'],
			'password'=>$data['password']);			
			$this->db->set($array);
			$this->db->where('id',$data['id']);
			$this->db->update('tb_admin');			
	}
	
	public function getDosen()
	{
		$query = $this->db->select('tb_matkul.nama_matkul, tb_dosen.id, tb_dosen.nama_dsn, tb_dosen.NIP, tb_dosen.no_telp, tb_dosen.email')
                 ->from('tb_dosen')
                 ->join('tb_matkul', 'tb_matkul.id_matkul = tb_dosen.id_matkul', 'inner')
                 ->get();
        return $query->result_array();
	}

	public function getKaprodiKK()
	{
		$query = $this->db->select('tb_jurusan.nama_jur, tb_kaprodikk.id_kaprodikk, tb_kaprodikk.nama, tb_kaprodikk.NIP, tb_kaprodikk.no_telp, tb_kaprodikk.email')
                 ->from('tb_kaprodikk')
                 ->join('tb_jurusan', 'tb_jurusan.id_jur = tb_kaprodikk.id_jur', 'inner')
                 ->get();
        return $query->result_array();
	}

	public function getMatkul()
	{
		$query = $this->db->select('tb_jurusan.nama_jur, tb_matkul.id_matkul, tb_matkul.kode_matkul, tb_matkul.nama_matkul')
                 ->from('tb_matkul')
                 ->join('tb_jurusan', 'tb_jurusan.id_jur = tb_matkul.id_jur', 'inner')
                 ->get();
        return $query->result_array();
	}

	public function getJadwal()
	{
		$query = $this->db->select('tb_ujian.id_ujian, tb_matkul.kode_matkul, tb_matkul.nama_matkul, tb_ujian.tgl_ujian, tb_ujian.jenis_ujian, tb_ujian.semester, tb_ujian.tahun_ajar, tb_ujian.ujian, tb_ujian.status')
                 ->from('tb_ujian')
				 ->join('tb_matkul', 'tb_matkul.id_matkul = tb_ujian.id_matkul', 'inner')
				 ->order_by('tb_ujian.id_ujian', 'DESC')
                 ->get();
        return $query->result_array();
	}

	public function getSoal($where1,$where2)
	{
		$query = $this->db->select('tb_ujian.id_ujian, tb_matkul.kode_matkul, tb_matkul.nama_matkul, tb_ujian.tgl_ujian, tb_ujian.jenis_ujian, tb_ujian.semester, tb_ujian.tahun_ajar')
                 ->from('tb_ujian')
				 ->join('tb_matkul', 'tb_matkul.id_matkul = tb_ujian.id_matkul', 'inner')
				 ->where($where1)
				 ->where($where2)
				 ->order_by('tb_ujian.id_ujian', 'DESC')
                 ->get();
        return $query->result_array();
	}

	public function getSoal2($where)
    {
        $query = $this->db->select('tb_ujian.id_ujian, tb_jurusan.nama_jur, tb_matkul.kode_matkul, tb_matkul.nama_matkul, tb_ujian.tgl_ujian, tb_ujian.jenis_ujian, tb_ujian.semester, tb_ujian.tahun_ajar, tb_ujian.ujian, tb_ujian.soal_docx, tb_ujian.soal_pdf, tb_ujian.status')
                 ->from('tb_ujian')
                 ->join('tb_matkul', 'tb_matkul.id_matkul = tb_ujian.id_matkul', 'inner')
                 ->join('tb_jurusan', 'tb_jurusan.id_jur = tb_matkul.id_jur', 'inner')
                 ->where($where)
                 ->get();
        return $query->result_array();
    }

	public function create($table,$data)
	{
		$query = $this->db->insert($table, $data);
		return $query;
	}

	public function delete($table,$where)
    {
        $query = $this->db->delete($table, $where);
        return $query;
    }

	public function getJurusan()
	{
		$query = $this->db->get('tb_jurusan');
		return $query->result_array();
	}

	public function getMahasiswaAdm()
	{
		$this->db->select('*');		
		$this->db->from('tb_jurusan');		
		$this->db->from('tb_kelas');
		$this->db->from('tb_fakultas');
		$this->db->from('tb_mahasiswa');
		$this->db->where('tb_mahasiswa.id_kel = tb_kelas.id_kelas AND tb_kelas.id_jur=tb_jurusan.id_jur AND tb_jurusan.id_fks=tb_fakultas.id_fks');
		$query = $this->db->get();
		return $query;
	}

}
