<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('AdminModel');
		$this->load->helper('download');
		if($this->session->userdata('level') == '' || $this->session->userdata('level') != 'admin'){
			redirect("Login/");
		}
	}

	public function index()
	{
		$this->load->view('adm_header');
		$this->load->view('adm_dashboard');
		$this->load->view('adm_footer');
	}

	public function profil()
	{
		$data['profile'] = $this->AdminModel->getAdmin()->row();
		// var_dump($data);
		$this->load->view('adm_header');
		$this->load->view('adm_profil',$data);
		$this->load->view('adm_footer');
	}

	public function akunDosen()
	{
		$data['dosen'] = $this->AdminModel->getDosen();				
		$this->load->view('adm_header');
		$this->load->view('adm_akunDosen',$data);
		$this->load->view('adm_footer');
	}

	public function tambahKaprodiKK()
	{
		$data['jur'] = $this->AdminModel->getJurusan();	
		//  var_dump($data);
		$this->load->view('adm_header');
		$this->load->view('adm_tambahKaprodikk',$data);
		$this->load->view('adm_footer');
	}

	public function tambahDosen()
	{
		$table = "tb_matkul";
		$data['matkul'] = $this->AdminModel->get_all($table);	
		//  var_dump($data);
		$this->load->view('adm_header');
		$this->load->view('adm_tambahDosen',$data);
		$this->load->view('adm_footer');
	}

	public function akunKaprodiKK()
	{
		$data['kaprodi'] = $this->AdminModel->getKaprodiKK();
		$this->load->view('adm_header');
		$this->load->view('adm_akunKaprodiKK',$data);
		$this->load->view('adm_footer');
	}

	public function jurusan()
	{
		$table = "tb_jurusan";
		$data['jurusan'] = $this->AdminModel->get_all($table);
		$this->load->view('adm_header');
		$this->load->view('adm_jurusan',$data);
		$this->load->view('adm_footer');
	}

	public function tambahJurusan()
	{
		$this->load->view('adm_header');
		$this->load->view('adm_tambahJurusan');
		$this->load->view('adm_footer');
	}

	public function createJurusan()
	{
		$table = "tb_jurusan";
		$data = array (
			'nama_jur' => $this->input->post('jurusan')
		);
		$this->AdminModel->create($table,$data);
		redirect('Admin/jurusan');
	}

	public function editJurusan($id)
	{
		$table = "tb_jurusan";
		$where = array (
			'id_jur' => $id
		);
		$data['jur'] = $this->AdminModel->get_where($table,$where);
		$this->load->view('adm_header');
		$this->load->view('adm_editJurusan',$data);
		$this->load->view('adm_footer');
	}

	public function updateJurusan($id)
	{
		$table = "tb_jurusan";
		$data = array (
			'nama_jur' => $this->input->post('jurusan')
		);
		$where = array (
			'id_jur' => $id
		);
		$this->AdminModel->update($table,$data,$where);
		redirect('Admin/jurusan');
	}

	public function hapusJurusan($id)
	{
		$table = "tb_jurusan";
		$where = array (
			'id_jur' => $id
		);
		$this->AdminModel->delete($table,$where);
		redirect('Admin/jurusan');
	}

	public function mataKuliah()
	{
		$data['matkul'] = $this->AdminModel->getMatkul();
		$this->load->view('adm_header');
		$this->load->view('adm_matkul',$data);
		$this->load->view('adm_footer');
	}

	public function tambahMatkul()
	{
		$table = "tb_jurusan";
		$data['jur'] = $this->AdminModel->get_all($table);
		$this->load->view('adm_header');
		$this->load->view('adm_tambahMatkul',$data);
		$this->load->view('adm_footer');
	}

	public function createMatkul()
	{
		$table = "tb_matkul";
		$data = array (
			'kode_matkul' => $this->input->post('kode'),
			'nama_matkul' => $this->input->post('matkul'),
			'id_jur' => $this->input->post('jurusan')
		);
		$this->AdminModel->create($table,$data);
		redirect('Admin/mataKuliah');
	}

	public function editMatkul($id)
	{
		$table1 = "tb_matkul";
		$where = array (
			'id_matkul' => $id
		);
		$data['matkul'] = $this->AdminModel->get_where($table1,$where);

		$table2 = "tb_jurusan";
		$data['jur'] = $this->AdminModel->get_all($table2);

		$this->load->view('adm_header');
		$this->load->view('adm_editMatkul',$data);
		$this->load->view('adm_footer');
	}

	public function updateMatkul($id)
	{
		$table = "tb_matkul";
		$data = array (
			'kode_matkul' => $this->input->post('kode'),
			'nama_matkul' => $this->input->post('matkul'),
			'id_jur' => $this->input->post('jurusan')
		);
		$where = array (
			'id_matkul' => $id
		);
		$this->AdminModel->update($table,$data,$where);
		redirect('Admin/mataKuliah');
	}

	public function hapusMatkul($id)
	{
		$table = "tb_matkul";
		$where = array (
			'id_matkul' => $id
		);
		$this->AdminModel->delete($table,$where);
		redirect('Admin/mataKuliah');
	}
	
	public function editAdmin()
	{
		$data['id'] = $this->input->post('id');
		$data['NIK'] = $this->input->post('nik');
		$data['nama'] = $this->input->post('nama');
		$data['username'] = $this->input->post('username');
		$data['password'] = $this->input->post('password');
		$this->AdminModel->updateAdmin($data);
		redirect('Admin/profil');
	}

	public function jadwalUjian()
	{
		$data['jadwal'] = $this->AdminModel->getJadwal();
		$this->load->view('adm_header');
		$this->load->view('adm_jadwalUjian',$data);
		$this->load->view('adm_footer');
	}

	public function tambahJadwal()
	{
		$table = "tb_matkul";
		$data['matkul'] = $this->AdminModel->get_all($table);
		$this->load->view('adm_header');
		$this->load->view('adm_tambahJadwalUjian',$data);
		$this->load->view('adm_footer');
	}

	public function createJadwal()
	{
		$table = "tb_ujian";
		$data = array (
			'id_matkul' => $this->input->post('matkul'),
			'tgl_ujian' => $this->input->post('tanggal'),
			'jenis_ujian' => $this->input->post('jenis'),
			'semester' => $this->input->post('semester'),
			'tahun_ajar' => $this->input->post('tahun'),
			'status' => "Menunggu"
		);
		$this->AdminModel->create($table,$data);
		redirect('Admin/jadwalUjian');
	}

	public function soalUjian()
	{
		$where1 = array (
			'ujian' => "Ya"
		);
		$where2 = array (
			'status' => "Disetujui"
		);
		$data['soal'] = $this->AdminModel->getSoal($where1,$where2);
		$this->load->view('adm_header');
		$this->load->view('adm_soalUjian',$data);
		$this->load->view('adm_footer');
	}

	public function createKaprodiKK()
	{
		$table = "tb_kaprodikk";
		$data = array (
			'NIP' => $this->input->post('nip'),
			'nama' => $this->input->post('nama'),
			'no_telp' => $this->input->post('telp'),
			'email' => $this->input->post('email'),
			'username' => $this->input->post('username'),
			'password' => $this->input->post('password'),
			'id_jur' => $this->input->post('jurusan')
		);
		$this->AdminModel->create($table,$data);
		redirect('Admin/akunKaprodiKK');
	}

	public function createDosen()
	{
		$table = "tb_dosen";
		$data = array (
			'NIP' => $this->input->post('nip'),
			'nama_dsn' => $this->input->post('nama'),
			'no_telp' => $this->input->post('telp'),
			'email' => $this->input->post('email'),
			'username' => $this->input->post('username'),
			'password' => $this->input->post('password'),
			'id_matkul' => $this->input->post('matkul')
		);
		$this->AdminModel->create($table,$data);
		redirect('Admin/akunDosen');
	}
		
	public function hapusKaprodiKK($id)
	{
		$table = "tb_kaprodikk";
		$where = array (
			'id_kaprodikk' => $id
		);
		$this->AdminModel->delete($table,$where);
		redirect('Admin/akunKaprodiKK');
	}

	public function hapusDosen($id)
	{
		$table = "tb_dosen";
		$where = array (
			'id' => $id
		);
		$this->AdminModel->delete($table,$where);
		redirect('Admin/akunDosen');
	}

	public function lihatSoal($id)
	{
		$where = array (
			'tb_ujian.id_ujian' => $id
		);
		$data['ujian'] = $this->AdminModel->getSoal2($where);

		$this->load->view('adm_header');
		$this->load->view('adm_lihatSoal',$data);
		$this->load->view('adm_footer');
	}

	public function downloadDOCX($id)
	{
		$table = "tb_ujian";
		$where = array(
			'id_ujian' => $id
		);
		$data['soal'] = $this->AdminModel->get_where($table,$where);

		$file = realpath("soal_ujian") ."\\". $data['soal'][0]['soal_docx'];
		force_download($file, NULL);
	}

	public function downloadPDF($id)
	{
		$table = "tb_ujian";
		$where = array(
			'id_ujian' => $id
		);
		$data['soal'] = $this->AdminModel->get_where($table,$where);

		$file = realpath("soal_ujian") ."\\". $data['soal'][0]['soal_pdf'];
		force_download($file, NULL);
	}

	public function viewBAV($id)
	{
		$table = "tb_verifikasiSoal";
		$where = array(
			'id_ujian' => $id
		);
		$data['bav'] = $this->AdminModel->get_where($table,$where);

		$this->load->view('adm_header');
		$this->load->view('adm_verifikasiSoal',$data);
		$this->load->view('adm_footer');
	}

	public function viewBAP($id)
	{
		$table = "tb_penyerahanSoal";
		$where = array(
			'id_ujian' => $id
		);
		$data['bap'] = $this->AdminModel->get_where($table,$where);

		$this->load->view('adm_header');
		$this->load->view('adm_penyerahanSoal',$data);
		$this->load->view('adm_footer');
	}
		
}
?>