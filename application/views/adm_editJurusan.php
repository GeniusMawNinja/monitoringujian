<?php
	foreach ($jur as $j){
		$id = $j['id_jur'];
		$jr = $j['nama_jur'];
	}
?>
<!-- Sidebar -->
<div class="row">
		<div class="col-md-3">
			<nav class="sidebar">
				<div class="row nav">
					<div class="col-md-12">
						<p id="navhead"><b>Halo, <?php echo $this->session->userdata('nama'); ?>!</b></p>
					</div>
				</div>
				<a href="<?php echo base_url(); ?>Admin">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Dashboard</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/profil">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Profil Admin</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/akunKaprodiKK">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Akun Kaprodi / KK</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/akunDosen">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Akun Dosen</p>
					</div>
				</div>
				</a>
				
				<div class="row nav1 active">
					<div class="col-md-12">
						<p id="nav">Program Studi</p>
					</div>
				</div>
				
				<a href="<?php echo base_url(); ?>Admin/mataKuliah">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Mata Kuliah</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/jadwalUjian">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Jadwal Ujian</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/soalUjian">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Soal Ujian</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/beritaAcara">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Berkas Berita Acara</p>
					</div>
				</div>
				</a>
			</nav>
		</div>

	<!-- Content -->	
	<div class="col-md-9 content">
		<br>
		<div class="row">
			<div class="col-md-12">
				<h4 id="title">Edit Jurusan</h4>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="panel">
				<div class="row">
					<div class="col-md-12">
						<p id="title"><b>Form Edit Jurusan</b></p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<form method="POST" action="<?php echo base_url(); ?>Admin/updateJurusan/<?php echo $id; ?>">
							<table cellpadding="8">
								<tr>
									<td><label>Nama Jurusan </label></td>
									<td width="450px"> <input type="text" class="form-control" name="jurusan" value="<?php echo $jr; ?>" required></td>
								</tr>
								<tr>
									<td></td>
									<td><br><input type="submit" name="submit" id="submit" class="btn btn-success" value="Save"> <button name="cancel" id="submit" class="btn btn-secondary" onclick="window.history.back();">Cancel</button></td>
								</tr>
							</table>
						</form> 
					</div>
				</div>
			</div>
		</div>
	</div>
</div>