<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{
		if($this->session->userdata('level') == "kaprodi"){
			redirect("Kaprodi/");
		}elseif($this->session->userdata('level') == "dosen"){
			redirect("Dosen/");
		}elseif($this->session->userdata('level') == "admin"){
			redirect("Admin/");
		}else{
			$this->load->view('login');
		}
	}

	public function kaprodi()
	{
		if($this->session->userdata('level') == "kaprodi"){
			redirect("Kaprodi/");
		}elseif($this->session->userdata('level') == "dosen"){
			redirect("Dosen/");
		}elseif($this->session->userdata('level') == "admin"){
			redirect("Admin/");
		}else{
			$this->load->view('loginKaprodi');
		}
	}

	public function dosen()
	{
		if($this->session->userdata('level') == "kaprodi"){
			redirect("Kaprodi/");
		}elseif($this->session->userdata('level') == "dosen"){
			redirect("Dosen/");
		}elseif($this->session->userdata('level') == "admin"){
			redirect("Admin/");
		}else{
			$this->load->view('loginDosen');
		}
	}

	public function admin()
	{
		if($this->session->userdata('level') == "kaprodi"){
			redirect("Kaprodi/");
		}elseif($this->session->userdata('level') == "dosen"){
			redirect("Dosen/");
		}elseif($this->session->userdata('level') == "admin"){
			redirect("Admin/");
		}else{
			$this->load->view('loginAdmin');
		}
	}

	public function loginKaprodi()
	{
		$this->load->model('model_login');
		if($this->input->post('submit')){
			$table = "tb_kaprodikk";
			$user = $this->input->post('username');
			$pass = $this->input->post('password');

			$data = array (
				'username' => $user,
				'password' => $pass);
			$query['data'] = $this->model_login->get_where_numRows($table,$data);

			if(($query['data']) > 0){
				$query1 = $this->model_login->get_where($table,$data);
				$id = $query1[0]['id_kaprodikk'];
				$nip = $query1[0]['NIP'];
				$nama = $query1[0]['nama'];
				$jurusan = $query1[0]['id_jur'];

				$data_session = array(
				'id' => $id,
				'nim' => $nip,
				'nama' => $nama,
				'jurusan' => $jurusan,
				'level' => "kaprodi");

				$this->session->set_userdata($data_session);
				redirect("Kaprodi/");
			}else{
	            echo "<script>alert('Login gagal! Username atau password salah.');window.location.href='kaprodi'</script>";
	        }
		}
	}

	public function loginDosen()
	{
		$this->load->model('model_login');
		if($this->input->post('submit')){
			$table = "tb_dosen";
			$user = $this->input->post('username');
			$pass = $this->input->post('password');

			$data = array (
				'username' => $user,
				'password' => $pass);
			$query['data'] = $this->model_login->get_where_numRows($table,$data);

			if(($query['data']) > 0){
				$query1 = $this->model_login->get_where($table,$data);
				$id = $query1[0]['id'];
				$nip = $query1[0]['NIP'];
				$nama = $query1[0]['nama_dsn'];
				$matkul = $query1[0]['id_matkul'];

				$data_session = array(
				'id' => $id,
				'nip' => $nip,
				'nama' => $nama,
				'matkul' => $matkul,
				'level' => "dosen");

				$this->session->set_userdata($data_session);
				redirect("Dosen/");
			}else{
	            echo "<script>alert('Login gagal! Username atau password salah.');window.location.href='dosen'</script>";
	        }
		}
	}

	public function loginAdmin()
	{
		$this->load->model('model_login');
		if($this->input->post('submit')){
			$table = "tb_admin";
			$user = $this->input->post('username');
			$pass = $this->input->post('password');

			$data = array (
				'username' => $user,
				'password' => $pass);
			$query['data'] = $this->model_login->get_where_numRows($table,$data);

			if(($query['data']) > 0){
				$query1 = $this->model_login->get_where($table,$data);
				$id = $query1[0]['id'];
				$nik = $query1[0]['NIK'];
				$nama = $query1[0]['nama'];

				$data_session = array(
				'id' => $id,
				'nip' => $nik,
				'nama' => $nama,
				'level' => "admin");

				$this->session->set_userdata($data_session);
				redirect("Admin/");
			}else{
	            echo "<script>alert('Login gagal! Username atau password salah.');window.location.href='admin'</script>";
	        }
		}
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect('Login/');
	}
}