<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dosen extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('DosenModel');
		$this->load->helper(array('form', 'url'));
		if($this->session->userdata('level') == '' || $this->session->userdata('level') != 'dosen'){
			redirect("Login/");
		}
	}

	public function index()
	{
		$this->load->view('dsn_header');
		$this->load->view('dsn_dashboard');
		$this->load->view('dsn_footer');
	}

	public function profil()
	{
		$table = "tb_dosen";
		$where = array (
			'id' => $this->session->userdata('id'));
		$data['query'] = $this->DosenModel->get_where($table,$where);

		$data['profil'] = $this->DosenModel->getProfil($where); 

		$this->load->view('dsn_header');
		$this->load->view('dsn_profil',$data);
		$this->load->view('dsn_footer');
	}

	public function updateProfil(){
		if($this->input->post('submit')){
			$table = "tb_dosen";
			$where = array (
				'id' => $this->session->userdata('id'));
			$data = array (
				'NIP' => $this->input->post('nip'),
			    'nama_dsn' => $this->input->post('nama'),
		        'email' => $this->input->post('email'),
				'username' => $this->input->post('username'),
				'password' => $this->input->post('password'));
			
		    $query = $this->DosenModel->update($table,$data,$where);

		    if(isset($query)){
		        echo "<script>alert('Profil berhasil disimpan.');window.location.href='profil'</script>";
		    }else{
		        echo "<script>alert('Profil gagal disimpan!');window.location.href='profil'</script>";
		    }
		}
	}

	public function jadwalUjian()
	{
		$table = "tb_dosen";
		$where1 = array (
			'id' => $this->session->userdata('id')
		);
		$data['dosen'] = $this->DosenModel->get_where($table,$where1);

		$where2 = array (
			'tb_ujian.id_matkul' => $data['dosen'][0]['id_matkul']
		);
		$where3 = array (
			'tb_ujian.ujian' => null
		);
		$data['jadwal'] = $this->DosenModel->getJadwal($where2,$where3);
		$this->load->view('dsn_header');
		$this->load->view('dsn_jadwalUjian',$data);
		$this->load->view('dsn_footer');
	}

	public function updateJadwal($id)
	{
		$table = "tb_ujian";
		$data = array (
			'ujian' => $this->input->post('ujian')
		);
		$where = array (
			'id_ujian' => $id
		);
		$this->DosenModel->update($table,$data,$where);
		redirect('Dosen/verifikasiSoal');
	}

	public function verifikasiSoal()
	{
		$where1 = array (
			'tb_ujian.id_matkul' => $this->session->userdata('matkul'),
			'tb_ujian.ujian' => "Ya",
			'tb_ujian.status' => "Menunggu"
		);
		$data1['ujian'] = $this->DosenModel->getSoal($where1);

		if($data1['ujian'] == null){
			redirect('Dosen/penyerahanSoal');
		}else{
			$table = "tb_verifikasiSoal";
			$where2 = array (
				'id_ujian' => $data1['ujian'][0]['id_ujian']
			);
			$data2['cek'] = $this->DosenModel->get_where_numRows($table,$where2);

			if($data2['cek'] == 0) {
				$where3 = array (
					'id_matkul' => $this->session->userdata('matkul')
				);
				$data1['prodi'] = $this->DosenModel->join_matkulJurusan($where3);
				$this->load->view('dsn_header');
				$this->load->view('dsn_verifikasiSoal',$data1);
				$this->load->view('dsn_footer');
			}else{
				redirect('Dosen/soalUjian');
			}
		}
	}

	public function uploadBAV($id)
	{
		$table = "tb_verifikasiSoal";
		$data = array (
			'matkul' => $this->input->post('matkul'),
			'kode_mk' => $this->input->post('kode_mk'),
			'prodi' => $this->input->post('prodi'),
			'tgl_pembahasan' => $this->input->post('tanggal'),
			'matkul_kode' => $this->input->post('nama/kode_mk'),
			'pilgan' => $this->input->post('pilgan'),
			'essay' => $this->input->post('essay'),
			'lainnya' => $this->input->post('lain'),
			'waktu_ujian' => $this->input->post('waktu'),
			'peserta_rapat' => $this->input->post('peserta'),
			'catatan' => $this->input->post('catatan'),
			'koordinator' => $this->session->userdata('nama'),
			'tanggal' => date("Y-m-d"),
			'id_ujian' => $id
		);
		$this->DosenModel->create($table,$data);
		redirect('Dosen/soalUjian');
	}

	public function soalUjian()
	{
		$where1 = array (
			'tb_ujian.id_matkul' => $this->session->userdata('matkul'),
			'tb_ujian.ujian' => "Ya",
			'tb_ujian.status' => "Menunggu"
		);
		$data['soal'] = $this->DosenModel->getSoal($where1);

		$table = "tb_verifikasiSoal";
		$where3 = array (
			'id_ujian' => $data['soal'][0]['id_ujian']
		);
		$data2['cek'] = $this->DosenModel->get_where_numRows($table,$where3);

		if($data2['cek'] != 0) {
			$this->load->view('dsn_header');
			$this->load->view('dsn_soalUjian',$data);
			$this->load->view('dsn_footer');
		}else{
			redirect('Dosen/verifikasiSoal');
		}
	}

	public function uploadSoal($id)
	{
		$config['upload_path']          = './soal_ujian/';
        $config['allowed_types']        = 'doc|docx|pdf';
		$config['max_size']             = 2048;
		$config['overwrite']			= true;

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('soalDOCX')){
			$file = $this->upload->data('file_name');
			$table = "tb_ujian";
			$data = array (
				'soal_DOCX' => $file
			);
			$where = array (
				'id_ujian' => $id
			);
			$this->DosenModel->update($table,$data,$where);

			$data = array('upload_data' => $this->upload->data());

			if ($this->upload->do_upload('soalPDF')){
				$file = $this->upload->data('file_name');
				$table = "tb_ujian";
				$data = array (
					'soal_PDF' => $file
				);
				$where = array (
					'id_ujian' => $id
				);
				$this->DosenModel->update($table,$data,$where);

				//$data = array('upload_data' => $this->upload->data());
				echo "<script>alert('Soal ujian berhasil diupload!');window.location.href='../penyerahanSoal'</script>";
			}else{
				//$error = array('error' => $this->upload->display_errors());
				echo "<script>alert('Gagal upload soal ujian!');window.location.href='../soalUjian'</script>";
			}
        }else{
			//$error = array('error' => $this->upload->display_errors());
			echo "<script>alert('Gagal upload soal ujian!');window.location.href='../soalUjian'</script>";
        }
	}

	public function penyerahanSoal(){
		$where1 = array (
			'tb_ujian.id_matkul' => $this->session->userdata('matkul'),
			'tb_ujian.ujian' => "Ya",
			'tb_ujian.status' => "Menunggu"
		);
		$data1['ujian'] = $this->DosenModel->getSoal($where1);

		if($data1['ujian'] == null){
			$where3 = array (
				'id_matkul' => $this->session->userdata('matkul')
			);
			$data1['prodi'] = $this->DosenModel->join_matkulJurusan($where3);
			$data1['done'] = array( 'status' => "Selesai" );
			$this->load->view('dsn_header');
			$this->load->view('dsn_penyerahanSoal',$data1);
			$this->load->view('dsn_footer');
		}else{
			$table = "tb_penyerahanSoal";
			$where2 = array (
				'id_ujian' => $data1['ujian'][0]['id_ujian']
			);
			$data2['cek'] = $this->DosenModel->get_where_numRows($table,$where2);

			if($data2['cek'] == 0) {
				$where3 = array (
					'id_matkul' => $this->session->userdata('matkul')
				);
				$data1['prodi'] = $this->DosenModel->join_matkulJurusan($where3);
				$data1['done'] = array( 'status' => "Belum" );
				$this->load->view('dsn_header');
				$this->load->view('dsn_penyerahanSoal',$data1);
				$this->load->view('dsn_footer');
			}else{
				$where3 = array (
					'id_matkul' => $this->session->userdata('matkul')
				);
				$data2['prodi'] = $this->DosenModel->join_matkulJurusan($where3);
				$data2['done'] = array( 'status' => "Selesai" );
				$this->load->view('dsn_header');
				$this->load->view('dsn_penyerahanSoal',$data2);
				$this->load->view('dsn_footer');
			}
		}
	}

	public function uploadBAP($id)
	{
		$jenis = $this->input->post('jenis');
		$tipe = $this->input->post('tipe');

		$table = "tb_penyerahanSoal";
		$data = array (
			'prodi' => $this->input->post('prodi'),
			'kode_mk' => $this->input->post('kode_mk'),
			'matkul' => $this->input->post('matkul'),
			'koordinator' => $this->session->userdata('nama'),
			'pilgan' => $jenis[0],
			'isian' => $jenis[1],
			'tipe1' => $tipe[0],
			'tipe2' => $tipe[1],
			'susulan' => $tipe[2],
			'remedial' => $tipe[3],
			'lainnya' => $tipe[4],
			'pembuat_soal' => $this->input->post('tim_pembuat'),
			'tanggal' => date("Y-m-d"),
			'id_ujian' => $id
		);
		$this->DosenModel->create($table,$data);

		$table = "tb_ujian";
		$data = array (
			'status' => "Diserahkan"
		);
		$where = array (
			'id_ujian' => $id
		);
		$this->DosenModel->update($table,$data,$where);
		redirect('Dosen/penyerahanSoal');
	}

}
?>