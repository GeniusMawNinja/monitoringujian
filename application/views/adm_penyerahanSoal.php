<?php
	foreach ($bap as $b){
		$id = $b['id'];
		$nama_mk = $b['matkul'];
		$kode_mk = $b['kode_mk'];
		$prodi = $b['prodi'];
		$pilgan = $b['pilgan'];	
		$isian = $b['isian'];
		$tipe1 = $b['tipe1'];
		$tipe2 = $b['tipe2'];
		$susulan = $b['susulan'];
		$remedial = $b['remedial'];
		$lainnya = $b['lainnya'];
		$pembuat = $b['pembuat_soal'];
		$tgl = $b['tanggal'];
		$koor = $b['koordinator'];	
	}
?>
<!-- Sidebar -->
<div class="row">
		<div class="col-md-3">
			<nav class="sidebar">
				<div class="row nav">
					<div class="col-md-12">
						<p id="navhead"><b>Halo, <?php echo $this->session->userdata('nama'); ?>!</b></p>
					</div>
				</div>
				<a href="<?php echo base_url(); ?>Admin">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Dashboard</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/profil">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Profil Admin</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/akunKaprodiKK">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Akun Kaprodi / KK</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/akunDosen">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Akun Dosen</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/jurusan">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Program Studi</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/mataKuliah">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Mata Kuliah</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/jadwalUjian">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Jadwal Ujian</p>
					</div>
				</div>
				</a>
				
				<div class="row nav2 active">
					<div class="col-md-12">
						<p id="nav">Soal Ujian</p>
					</div>
				</div>
				
				<a href="<?php echo base_url(); ?>Admin/beritaAcara">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Berkas Berita Acara</p>
					</div>
				</div>
				</a>
			</nav>
		</div>

	<!-- Content -->	
	<div class="col-md-9 content">
		<br>
		<div class="row">
			<div class="col-md-12">
				<h4 id="title">Berita Acara</h4>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="panel">
				<div class="row">
					<div class="col-md-12">
						<p id="title"><b>Berita Acara Penyerahan Soal Ujian</b></p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<form method="POST" action="<?php echo base_url(); ?>Dosen/uploadBAP/<?php echo $id; ?>">
							<table cellpadding="8">
								<tr>
									<td width="250px"><label><b>Fakultas </b></label></td>
									<td><label><b>: Rekayasa Industri </b></label></td>
								</tr>
								<tr>
									<td><label>Program Studi </label></td>
									<td><input type="text" class="form-control" name="prodi" value="<?php echo $prodi; ?>" required disabled></td>
								</tr>
								<tr>
									<td><label>Kode MK </label></td>
									<td><input type="text" class="form-control" name="kode_mk" value="<?php echo $kode_mk; ?>" required disabled></td>
								</tr>
								<tr>
									<td><label>Nama MK </label></td>
									<td><input type="text" class="form-control" name="matkul" value="<?php echo $nama_mk; ?>" required disabled></td>
								</tr>
								<tr>
									<td><label>Koordinator MK </label></td>
									<td><input type="text" class="form-control" name="koordinator" value="<?php echo $koor; ?>" required disabled></td>
								</tr>
								<tr>
									<td><label>Jenis Soal </label></td>
									<td>
										<div class="form-check form-check-inline"><input class="form-check-input" type="checkbox" name="jenis[]" value="Ya" id="jenis1" <?php if($pilgan=="Ya"){echo "checked";} ?> disabled><label class="form-check-label" for="jenis1">Pilihan Ganda</label></div>
										<div class="form-check form-check-inline"><input class="form-check-input" type="checkbox" name="jenis[]" value="Ya" id="jenis2" <?php if($isian=="Ya"){echo "checked";} ?> disabled><label class="form-check-label" for="jenis2">Isian</label></div>
									</td>
								</tr>
								<tr>
									<td><label>Tipe Soal </label></td>
									<td>
										<div class="form-check form-check-inline"><input class="form-check-input" type="checkbox" name="tipe[]" value="Ya" id="tipe1" <?php if($tipe1=="Ya"){echo "checked";} ?> disabled><label class="form-check-label" for="tipe1">Tipe 1</label></div>
										<div class="form-check form-check-inline"><input class="form-check-input" type="checkbox" name="tipe[]" value="Ya" id="tipe2" <?php if($tipe2=="Ya"){echo "checked";} ?> disabled><label class="form-check-label" for="tipe2">Tipe 2</label></div>
										<div class="form-check form-check-inline"><input class="form-check-input" type="checkbox" name="tipe[]" value="Ya" id="tipe3" <?php if($susulan=="Ya"){echo "checked";} ?> disabled><label class="form-check-label" for="tipe3">Susulan</label></div>
										<div class="form-check form-check-inline"><input class="form-check-input" type="checkbox" name="tipe[]" value="Ya" id="tipe4" <?php if($remedial=="Ya"){echo "checked";} ?> disabled><label class="form-check-label" for="tipe4">Remedial</label></div>
										<div class="form-check form-check-inline"><input class="form-check-input" type="checkbox" name="tipe[]" value="Ya" id="tipe5" <?php if($lainnya=="Ya"){echo "checked";} ?> disabled><label class="form-check-label" for="tipe5">Lainnya</label></div>
									</td>
								</tr>
								<tr>
									<td><label>Tim Pembuat Soal </label></td>
									<td><textarea class="form-control" name="tim_pembuat" required disabled><?php echo $pembuat; ?></textarea></td>
								</tr>
								<tr>
									<td colspan="2"><label><b>SOAL YANG DISERAHKAN TELAH SESUAI DENGAN FORMAT DAN SIAP UNTUK DIGANDAKAN.</b></label></td>
								</tr>
								<tr>
									<td><label>Tanggal </label></td>
									<td><label>: <?php echo $tgl; ?></label></td>
								</tr>
								<tr>
									<td></td>
									<td><br><a href="<?php echo base_url();?>Kaprodi/downloadBAP/<?php echo $id;?>"><button type="button" class="btn btn-success">Download BAP</button></a></td>
								</tr>
							</table>
						</form> 
						
					</div>
				</div>
			</div>
		</div>
		<br><br><br>
	</div>
</div>