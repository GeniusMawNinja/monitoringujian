<?php 
	foreach($jurusan as $j){
		$jur = $j['nama_jur'];
	}
?>
<!-- Sidebar -->
<div class="row">
		<div class="col-md-3">
			<nav class="sidebar">
				<div class="row nav">
					<div class="col-md-12">
						<p id="navhead"><b>Halo, <?php echo $this->session->userdata('nama'); ?>!</b></p>
					</div>
				</div>
				<a href="<?php echo base_url(); ?>Kaprodi">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Dashboard</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Kaprodi/profil">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Profil Kaprodi / KK</p>
					</div>
				</div>
				</a>
				
				<div class="row nav1 active">
					<div class="col-md-12">
						<p id="nav">Ujian</p>
					</div>
				</div>
				
			</nav>
		</div>

	<!-- Content -->	
	<div class="col-md-9 content">
		<br>
			<div class="row">
				<div class="col-md-9">
					<h4 id="title"><b>Ujian</b> <?php echo $jur; ?></h4>
				</div>
			</div>
			<br>
			<div class="row">
			<div class="col-md-11">
				<table class="table">
					<thead class="thead-light">
						<tr>
							<th>Kode Matkul</th>
							<th>Mata Kuliah</th>
							<th>Tgl. Ujian</th>
							<th>Periode</th>
							<th>Diujikan?</th>
							<th>Status</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
				<?php 	foreach($soal as $s) { ?>
						<tr>
							<td><?php echo $s['kode_matkul']; ?></td>
							<td><?php echo $s['nama_matkul']; ?></td>
							<td><?php echo $s['tgl_ujian']; ?></td>
							<td><?php echo $s['jenis_ujian'] ." ". $s['semester'] ." ". $s['tahun_ajar']; ?></td>
							<td><?php echo $s['ujian']; ?></td>
							<td><?php echo $s['status']; ?></td>
							<td><a href="<?php echo base_url(); ?>Kaprodi/detailUjian/<?php echo $s['id_ujian']; ?>"><?php if($s['status']=="Diserahkan" || $s['status']=="Disetujui"){echo "Detail";}?></a></td>
						</tr>
				<?php } ?>
					</tbody>
				</table>
			</div>
			<div class="col-md-1"></div>
		</div>
		<br><br><br>
	</div>
</div>