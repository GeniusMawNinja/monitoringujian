<?php
	$id = "";
	foreach ($ujian as $u){
		$id = $u['id_ujian'];
		$nama_mk = $u['nama_matkul'];
		$kode_mk = $u['kode_matkul'];
		$tgl = $u['tgl_ujian'];
		$periode = $u['jenis_ujian'] ." Semester ". $u['semester'] ." Tahun Akademik ". $u['tahun_ajar'];
	}

	foreach ($prodi as $p ){
		$id_jur = $p['id_jur'];
		$nama_jur = $p['nama_jur'];
	}
?>
<!-- Sidebar -->
<div class="row">
		<div class="col-md-3">
			<nav class="sidebar">
				<div class="row nav">
					<div class="col-md-12">
						<p id="navhead"><b>Halo, <?php echo $this->session->userdata('nama'); ?>!</b></p>
					</div>
				</div>
				<a href="<?php echo base_url(); ?>Dosen">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Dashboard</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Dosen/profil">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Profil Dosen</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Dosen/jadwalUjian">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Jadwal Ujian</p>
					</div>
				</div>
				</a>
				
				<div class="row nav2 active">
					<div class="col-md-12">
						<p id="nav">Soal Ujian</p>
					</div>
				</div>
				
			</nav>
		</div>

	<!-- Content -->	
	<div class="col-md-9 content">
		<br>
		<div class="row">
			<div class="col-md-12">
				<h4 id="title">Berita Acara</h4>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="panel">
				<?php if($id != "") { ?>
				<div class="row">
					<div class="col-md-12">
						<p id="title"><b>Berita Acara Verifikasi Soal <?php echo $periode; ?> </b></p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<form method="POST" action="<?php echo base_url(); ?>Dosen/uploadBAV/<?php echo $id; ?>">
							<table cellpadding="8">
								<tr>
									<td width="300px"><label><b>Fakultas </b></label></td>
									<td><label><b>: Rekayasa Industri </b></label></td>
								</tr>
								<tr>
									<td><label>Saya sebagai Koordinator Mata Kuliah </label></td>
									<td><input type="text" class="form-control" name="matkul" value="<?php echo $nama_mk; ?>" required></td>
								</tr>
								<tr>
									<td><label>Kode MK </label></td>
									<td><input type="text" class="form-control" name="kode_mk" value="<?php echo $kode_mk; ?>" required></td>
								</tr>
								<tr>
									<td><label>Program Studi </label></td>
									<td><input type="text" class="form-control" name="prodi" value="<?php echo $nama_jur; ?>" required></td>
								</tr>
								<tr>
									<td colspan="2"><label>Menyatakan bahwa telah dilakukan pembahasan antara Dosen Koordinator dan Dosen Mata Kuliah paralel : </label></td>
								</tr>
								<tr>
									<td><label>Tanggal Pembahasan </label></td>
									<td width="500px"><input type="date" class="form-control" name="tanggal" required></td>
								</tr>
								<tr>
									<td><label>Mata Kuliah / Kode MK </label></td>
									<td><input type="text" class="form-control" name="nama/kode_mk" value="<?php echo $nama_mk ." / ". $kode_mk; ?>" required></td>
								</tr>
								<tr>
									<td colspan="2"><label>Jumlah Soal : </label></td>
								</tr>
								<tr>
									<td><label>Pilihan Ganda </label></td>
									<td><input type="number" class="form-control" name="pilgan"></td>
								</tr>
								<tr>
									<td><label>Essay </label></td>
									<td><input type="number" class="form-control" name="essay"></td>
								</tr>
								<tr>
									<td><label>Lain-lain </label></td>
									<td><input type="number" class="form-control" name="lain"></td>
								</tr>
								<tr>
									<td colspan="2"></td>
								</tr>
								<tr>
									<td><label>Waktu Ujian (menit) </label></td>
									<td><input type="number" class="form-control" name="waktu"></td>
								</tr>
								<tr>
									<td><label>Peserta Rapat Koordinasi </label></td>
									<td><textarea class="form-control" name="peserta" required></textarea></td>
								</tr>
								<tr>
									<td colspan="2"><label>Soal ujian telah diverivikasi dan sesuai dengan tujuan kompetensi mata kuliah. Beban/alokasi waktu pengerjaan soal yang diberikan sesuai dengan tingkat kesulitan soal. </label></td>
								</tr>
								<tr>
									<td><label>Catatan </label></td>
									<td><textarea class="form-control" name="catatan"></textarea></td>
								</tr>
								<tr>
									<td></td>
									<td><br><input type="submit" name="submit" id="submit" class="btn btn-success" value="Submit"></td>
								</tr>
							</table>
						</form> 
						
					</div>
				</div>
				<?php } else{ ?>
				<div class="row">
					<div class="col-md-12">
						<p id="title"><b>Proses monitoring ujian telah selesai, silahkan tunggu informasi selanjutnya.</b></p>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
		<br><br><br>
	</div>
</div>